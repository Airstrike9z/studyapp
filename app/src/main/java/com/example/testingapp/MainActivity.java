package com.example.testingapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.util.Pair;
import android.view.View;

import com.example.testingapp.Library.Flashcards.FCUnit;
import com.example.testingapp.Library.Fragments.StartingFragment;
import com.example.testingapp.Library.Fragments.TimerStartFragment;
import com.example.testingapp.Library.PracticeTests.PTUnit;
import com.example.testingapp.Unique.Flashcard;
import com.example.testingapp.Unique.Question;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

/**

    App developed by Team Hello World.
    Open README under com.example.testingapp for an explanation on how the app works!

 */

public class MainActivity extends AppCompatActivity {
    // vars
    public ArrayList<FCUnit> fcUnits = new ArrayList<>();
    public ArrayList<PTUnit> ptUnits = new ArrayList<>();
    public ArrayList<Flashcard> currentFCCards = new ArrayList<>();
    public ArrayList<Question> currentPTQuestions = new ArrayList<>();
    public ArrayList<DatabaseReference> allFCDbRefs = new ArrayList<>();
    public ArrayList<DatabaseReference> allPTDbRefs = new ArrayList<>();
    public FCUnit currentFCUnit;
    public PTUnit currentPTUnit;
    public ArrayList<Object> favourites = new ArrayList<>();
    public ArrayList<RecyclerView.Adapter> adapters = new ArrayList<>();
    private long studyTimeMilli = 0;
    public long breakTimeMilli = 0;
    public String timerTag = "";
    public boolean timerActive = false;
    public long startTime = 0;
    public static String FCUNITS = "FCUnits";
    public static String PTUNITS = "PTUnits";
    public static String PRACTICETESTS = "PracticeTests";
    public static String FLASHCARDS = "Flashcards";
    public static String OWNER = "Owner";
    public static String NAME = "Name";
    public static String ANSWER = "Answer";
    public static String QUESTION = "Question";
    public static String LEVELS = "Levels";
    public static String VIEWER = "Viewer";
    public static String CODE = "Code";
    public static String FAVORITE = "FavoritedBy";
    public static int LENGTHOFCODE = 6;
    public String owner = "None"; // None by default

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_screen);
        open_screen(new StartingFragment());
    }

    public void startDatabase() {
        DatabaseReference dbMain = FirebaseDatabase.getInstance().getReference();

        dbMain.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                // clear all lists to prevent duplication
                fcUnits.clear();
                ptUnits.clear();
                currentFCCards.clear();
                currentPTQuestions.clear();
                allFCDbRefs.clear();
                allPTDbRefs.clear();
                favourites.clear();

                // loop through FCUnits to grab every flashcard unit
                for (DataSnapshot child : snapshot.child(FCUNITS).getChildren()){
                    ArrayList<Object> tmpFavourites = new ArrayList<>();
                    boolean addToFav = false;
                    allFCDbRefs.add(child.getRef());
                    FCUnit unit = new FCUnit(child.getRef());
                    unit.setUnitName(String.valueOf(child.child(NAME).getValue()));
                    unit.setAuthorName(String.valueOf(child.child(OWNER).getValue()));
                    String unitCode = child.getKey().substring(1,LENGTHOFCODE+1).toLowerCase();
                    unit.setCode(unitCode);

                    // check if current user (MainActivity.owner) has favourited this FC unit
                    for (DataSnapshot dbFavoriteUser : child.child(FAVORITE).getChildren()) {
                        if (String.valueOf(dbFavoriteUser.getValue()).equals(owner)) {
                            unit.addFavoriteUser(owner);
                            addToFav = true;
                            break;
                        }
                    }

                    // loop through unit's flashcards list to grab every flashcard
                    for (DataSnapshot dbCard : child.child(FLASHCARDS).getChildren()) {
                        Flashcard card = new Flashcard(dbCard.getRef());
                        card.setQuestion(String.valueOf(dbCard.child(QUESTION).getValue()));
                        card.setAnswer(String.valueOf(dbCard.child(ANSWER).getValue()));

                        // set current flashcard's level to corresponding lvl in database
                        for (DataSnapshot dbLvlUser : dbCard.child(LEVELS).getChildren()) {
                            if (dbLvlUser.getKey().equals(owner)) {
                                card.setLevel(Integer.parseInt(String.valueOf(dbLvlUser.getValue())));
                            }
                            card.addDbLevel(dbLvlUser.getKey(),dbLvlUser.getValue());
                        }
                        card.setCurrentUser(owner);

                        // add current flashcard to unit's flashcard list
                        unit.addFlashcard(card);

                        if (currentFCUnit != null && unit.getRef().equals(currentFCUnit.getRef())) {
                            currentFCCards.add(card);
                        }
                    }

                    if (addToFav) {
                        tmpFavourites.add(unit);
                    }
                    // check if StartingFragment entered user (MainActivity.owner) is the owner of current FC unit
                    if (owner.equals(child.child(OWNER).getValue().toString())) {
                        favourites.addAll(tmpFavourites);
                        fcUnits.add(unit);
                    }
                    else {
                        // check if StartingFragment entered user (MainActivity.owner) has access to each fcUnit
                        for (DataSnapshot dbViewer : child.child(VIEWER).getChildren()) {
                            if (owner.equals(dbViewer.getValue().toString())) {
                                favourites.addAll(tmpFavourites);
                                fcUnits.add(unit);
                                break;
                            }
                        }
                    }

                    // notify adapters
                    updateAdapters();
                }

                // loop through PTUnits to grab every practice test unit
                for (DataSnapshot child : snapshot.child(PTUNITS).getChildren()){
                    ArrayList<Object> tmpFavourites = new ArrayList<>();
                    boolean addToFav = false;
                    allPTDbRefs.add(child.getRef());
                    PTUnit unit = new PTUnit(child.getRef());
                    unit.setUnitName(String.valueOf(child.child(NAME).getValue()));
                    unit.setAuthorName(String.valueOf(child.child(OWNER).getValue()));
                    String unitCode = child.getKey().substring(1,LENGTHOFCODE+1).toLowerCase();
                    unit.setCode(unitCode);

                    // check if current user (MainActivity.owner) has favourited this PT unit
                    for (DataSnapshot dbFavoriteUser : child.child(FAVORITE).getChildren()) {
                        if (String.valueOf(dbFavoriteUser.getValue()).equals(owner)) {
                            unit.addFavoriteUser(owner);
                            addToFav = true;
                            break;
                        }
                    }

                    // loop through unit's Questions list to grab every question
                    for (DataSnapshot dbQuestion : child.child(PRACTICETESTS).getChildren()) {
                        Question question = new Question(dbQuestion.getRef());
                        question.setQuestion(String.valueOf(dbQuestion.child(QUESTION).getValue()));
                        question.setAnswer(String.valueOf(dbQuestion.child(ANSWER).getValue()));
                        unit.addQuestion(question);

                        if (currentPTUnit != null && unit.getRef().equals(currentPTUnit.getRef())) {
                            currentPTQuestions.add(question);
                        }
                    }

                    if (addToFav) {
                        tmpFavourites.add(unit);
                    }
                    // check if StartingFragment entered user (MainActivity.owner) is the owner of current PT unit
                    if (owner.equals(child.child(OWNER).getValue().toString())) {
                        favourites.addAll(tmpFavourites);
                        ptUnits.add(unit);
                    }
                    else {
                        // check if StartingFragment entered user (MainActivity.owner) has access to each ptUnit
                        for (DataSnapshot dbViewer : child.child(VIEWER).getChildren()) {
                            if (owner.equals(dbViewer.getValue().toString())) {
                                favourites.addAll(tmpFavourites);
                                ptUnits.add(unit);
                                break;
                            }
                        }
                    }

                    // notify adapters
                    updateAdapters();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) { }
        });
    }

    public void open_screen(Fragment fragment) {
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        fragmentTransaction.replace(R.id.frameLayout, fragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    public void open_screen(Fragment fragment,String tag) {
        timerTag = tag;
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        fragmentTransaction.add(R.id.frameLayout,fragment,tag);
        fragmentTransaction.commit();
    }

    public void showTimer() {
        FragmentManager fm = getSupportFragmentManager();
        fm.popBackStack(); //timerTag,0
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        fragmentTransaction.show(fm.findFragmentByTag(timerTag));
        fragmentTransaction.commit();
    }

    public void hideTimer() {
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        fragmentTransaction.hide(fm.findFragmentByTag(timerTag));
        fragmentTransaction.commit();
    }

    public void setTime(long mili) {
        studyTimeMilli = mili;
    }

    public long getTime() {
        System.out.println(this.studyTimeMilli);
        return this.studyTimeMilli;
    }

//    public void showTimer() {
//        FragmentManager fm = getSupportFragmentManager();
//        fm.popBackStack(timerTag,0);
//    }

    public void addAdapter(RecyclerView.Adapter adapter) {
        adapters.add(adapter);
    }

    public void removeAdapter(RecyclerView.Adapter adapter) {
        adapters.remove(adapter);
    }

    public void updateAdapters() {
        for (RecyclerView.Adapter adapter : adapters){
            adapter.notifyDataSetChanged();
        }
    }

    public void addFCUnit(FCUnit unit) {
        DatabaseReference dbFCUnit = FirebaseDatabase.getInstance().getReference().child(FCUNITS).push();
        dbFCUnit.child(OWNER).setValue(unit.getAuthorName());
        dbFCUnit.child(NAME).setValue(unit.getUnitName());
        updateAdapters();
    }

    public void addPTUnit(PTUnit unit) {
        DatabaseReference dbPTUnit = FirebaseDatabase.getInstance().getReference().child(PTUNITS).push();
        dbPTUnit.child(OWNER).setValue(unit.getAuthorName());
        dbPTUnit.child(NAME).setValue(unit.getUnitName());
        updateAdapters();
    }

    public void addFlashcard(FCUnit unit, Flashcard card) {
        DatabaseReference dbFCUnitCard = FirebaseDatabase.getInstance().getReference().child(FCUNITS).child(unit.getRef().getKey()).child(FLASHCARDS).push();
        dbFCUnitCard.child(QUESTION).setValue(card.getQuestion());
        dbFCUnitCard.child(ANSWER).setValue(card.getAnswer());
        updateAdapters();
    }

    public void addQuestion(PTUnit unit, Question question) {
        DatabaseReference dbPTUnitCard = FirebaseDatabase.getInstance().getReference().child(PTUNITS).child(unit.getRef().getKey()).child(PRACTICETESTS).push();
        dbPTUnitCard.child(QUESTION).setValue(question.getQuestion());
        dbPTUnitCard.child(ANSWER).setValue(question.getAnswer());
        updateAdapters();
    }

    public void setCurrentFCUnit(FCUnit unit) {
        currentFCUnit = unit;
        currentFCCards = unit.getFlashcards();
    }

    public void setCurrentPTUnit(PTUnit unit) {
        currentPTUnit = unit;
        currentPTQuestions = unit.getQuestions();
    }
}