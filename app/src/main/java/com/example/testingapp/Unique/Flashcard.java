package com.example.testingapp.Unique;

import com.example.testingapp.MainActivity;
import com.google.firebase.database.DatabaseReference;

import java.util.HashMap;
import java.util.Map;

public class Flashcard {
    private String currentUser;
    private Map<String,Object> dbLevelsList = new HashMap<>();
    private String question;
    private String answer;
    private int level = 1;
    private DatabaseReference dbRef = null;

    public Flashcard(){}

    public Flashcard(DatabaseReference dbRef){
        this.dbRef = dbRef;
    }

    public Flashcard(Flashcard flashcard) {
        this.question = flashcard.getQuestion();
        this.answer = flashcard.getAnswer();
        this.level = flashcard.getLevel();
        this.dbRef = flashcard.getRef();
    }

    public String getQuestion() {
        return this.question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getAnswer() {
        return this.answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public int getLevel() {
        return this.level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public void removeLevel() {
        if (this.level > 1) {
            this.level--;
        }
    }

    public void addLevel() {
        if (this.level < 4) {
            this.level++;
        }
    }

    public void remove() {
        this.dbRef.removeValue();
    }

    public void update() {
        Map<String,Object> map = new HashMap<>();
        map.put(MainActivity.QUESTION,this.question);
        map.put(MainActivity.ANSWER,this.answer);
        this.dbRef.updateChildren(map);
    }

    public void updateLevels() {
        Map<String,Object> map = new HashMap<>();
        this.dbLevelsList.put(currentUser,this.level);
        map.put(MainActivity.LEVELS,this.dbLevelsList);
        this.dbRef.updateChildren(map);
    }

    public DatabaseReference getRef() {
        return dbRef;
    }

    public void addDbLevel(String key, Object val) {
        this.dbLevelsList.put(key,val);
    }

    public void setCurrentUser(String name) {
        this.currentUser = name;
    }
}
