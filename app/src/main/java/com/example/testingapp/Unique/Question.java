package com.example.testingapp.Unique;

import com.example.testingapp.MainActivity;
import com.google.firebase.database.DatabaseReference;

import java.util.HashMap;
import java.util.Map;

public class Question {
    private String question;
    private String answer;
    private DatabaseReference dbRef = null;

    public Question(){
    }

    public Question(DatabaseReference dbRef){
        this.dbRef = dbRef;
    }

    public Question(Question question) {
        this.question = question.getQuestion();
        this.answer = question.getAnswer();
        this.dbRef = question.getRef();
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public void remove() {
        this.dbRef.removeValue();
    }

    public void update() {
        Map<String,Object> map = new HashMap<>();
        map.put(MainActivity.QUESTION,this.question);
        map.put(MainActivity.ANSWER,this.answer);
        this.dbRef.updateChildren(map);
    }

    public DatabaseReference getRef() {
        return dbRef;
    }
}
