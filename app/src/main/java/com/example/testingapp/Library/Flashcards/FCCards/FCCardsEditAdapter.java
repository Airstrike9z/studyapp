package com.example.testingapp.Library.Flashcards.FCCards;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.example.testingapp.Library.Flashcards.FCDecks.FCDecksEditHolder;
import com.example.testingapp.Library.Flashcards.FCUnit;
import com.example.testingapp.MainActivity;
import com.example.testingapp.R;
import com.example.testingapp.Unique.Flashcard;

import java.util.ArrayList;

public class FCCardsEditAdapter extends RecyclerView.Adapter<FCCardsEditHolder>{

    private Context context;
    private ArrayList<Flashcard> flashcards = new ArrayList<>();
    private ArrayList<Flashcard> cardsToRemove = new ArrayList<>();
    MainActivity mainActivity;
    Fragment fragment;

    public FCCardsEditAdapter(@NonNull Context context, @NonNull ArrayList<Flashcard> flashcards, @NonNull Fragment fragment) {
        this.context = context;
        this.flashcards.addAll(flashcards);
        this.fragment = fragment;
    }

    @NonNull
    @Override
    public FCCardsEditHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View itemView = inflater.inflate(R.layout.item_edit_card, parent, false);
        FCCardsEditHolder viewHolder = new FCCardsEditHolder(itemView);
        return viewHolder;
    }

    public void setMainActivity(MainActivity mainActivity) {
        this.mainActivity = mainActivity;
    }

    @Override
    public void onBindViewHolder(@NonNull FCCardsEditHolder holder, int position) {
        Flashcard card = flashcards.get(position);

        TextWatcher questionWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                flashcards.get(position).setQuestion(s.toString());
            }
        };

        holder.cardQuestion.addTextChangedListener(questionWatcher);

        holder.cardAnswer.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                flashcards.get(position).setAnswer(s.toString());
            }
        });

        holder.duplicateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Flashcard newCard = new Flashcard();
                newCard.setQuestion(new String(flashcards.get(position).getQuestion()));
                newCard.setAnswer(new String(flashcards.get(position).getAnswer()));
                flashcards.add(newCard);
                notifyDataSetChanged();
            }
        });

        holder.deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cardsToRemove.add(card);
                flashcards.remove(card);
                notifyDataSetChanged();
            }
        });

        holder.cardQuestion.setText(card.getQuestion());
        holder.cardAnswer.setText(card.getAnswer());
    }

    @Override
    public int getItemCount() {
        return flashcards.size();
    }

    public ArrayList<Flashcard> getData() {
        return flashcards;
    }

    public void confirmEdit(FCUnit unit) {
        for (Flashcard card : cardsToRemove) {
            card.remove();
        }
        for (Flashcard flashcard : this.flashcards) {
            if (flashcard.getRef() == null) {
                mainActivity.addFlashcard(unit,flashcard);
            }
            else {
                flashcard.update();
            }
        }
    }

    public void addData() {
        Flashcard card = new Flashcard();
        card.setQuestion("New question");
        card.setAnswer("New answer");
        this.flashcards.add(card);
        notifyDataSetChanged();
    }

}
