package com.example.testingapp.Library.Fragments;

import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.example.testingapp.MainActivity;
import com.example.testingapp.R;
import com.example.testingapp.Unique.Flashcard;

import java.util.ArrayList;
import java.util.Collections;

public class FCFragment extends Fragment {
    View view;
    ArrayList<Flashcard> cards = new ArrayList<>();
    int currentIndex = 0;

    public FCFragment(ArrayList<Flashcard> viewCards) {
        cards.addAll(viewCards);
        Collections.shuffle(cards);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fc_study_screen, container, false);

        ImageButton backButton = view.findViewById(R.id.back_button);
        TextView deckName = view.findViewById(R.id.deck_name);
        ImageButton previousButton = view.findViewById(R.id.previous_button);
        ImageButton nextButton = view.findViewById(R.id.next_button);
        ImageView cardBackground = view.findViewById(R.id.card_background);
        TextView cardText = view.findViewById(R.id.card_text);
        TextView tapToViewText = view.findViewById(R.id.tap_to_view_text);
        TextView correctIncorrectText = view.findViewById(R.id.correct_incorrect_text);
        ImageButton incorrectButtonHighlighted = view.findViewById(R.id.incorrect_button_highlighted);
        ImageButton incorrectButtonUnhighlighted = view.findViewById(R.id.incorrect_button_unhighlighted);
        ImageButton correctButtonHighlighted = view.findViewById(R.id.correct_button_highlighted);
        ImageButton correctButtonUnhighlighted = view.findViewById(R.id.correct_button_unhighlighted);

        ImageButton homePageButton = view.findViewById(R.id.unhome_button);
        ImageButton timerButton = view.findViewById(R.id.untimer_button);

        ImageView lvl1 = view.findViewById(R.id.proficiency1_image);
        ImageView lvl2 = view.findViewById(R.id.proficiency2_image);
        ImageView lvl3 = view.findViewById(R.id.proficiency3_image);
        ImageView lvl4 = view.findViewById(R.id.proficiency4_image);

        deckName.setText(((MainActivity)getActivity()).currentFCUnit.getUnitName());
        cardText.setText(cards.get(currentIndex).getQuestion());
        setCardLvl(cards.get(currentIndex).getLevel(),lvl1,lvl2,lvl3,lvl4);

        homePageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity)getActivity()).open_screen(new HomeFragment());
            }
        });

        timerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((MainActivity)getActivity()).timerActive) {
                    ((MainActivity)getActivity()).showTimer();
                }
                else {
                    ((MainActivity)getActivity()).open_screen(new TimerPrepFragment());
                }
            }
        });

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager().popBackStack();
            }
        });

        previousButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (currentIndex > 0) {
                    currentIndex -= 1;
                    tapToViewText.setVisibility(View.VISIBLE);
                    correctIncorrectText.setVisibility(View.INVISIBLE);
                    incorrectButtonHighlighted.setVisibility(View.INVISIBLE);
                    incorrectButtonUnhighlighted.setVisibility(View.INVISIBLE);
                    correctButtonHighlighted.setVisibility(View.INVISIBLE);
                    correctButtonUnhighlighted.setVisibility(View.INVISIBLE);
                    cardText.setText(cards.get(currentIndex).getQuestion());

                    setCardLvl(cards.get(currentIndex).getLevel(),lvl1,lvl2,lvl3,lvl4);
                }
                else {
                    Toast.makeText(getActivity(), "This is the first flashcard in the deck!", Toast.LENGTH_SHORT).show();
                }
            }
        });

        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (currentIndex < cards.size()-1) {
                    currentIndex += 1;
                    tapToViewText.setVisibility(View.VISIBLE);
                    correctIncorrectText.setVisibility(View.INVISIBLE);
                    incorrectButtonHighlighted.setVisibility(View.INVISIBLE);
                    incorrectButtonUnhighlighted.setVisibility(View.INVISIBLE);
                    correctButtonHighlighted.setVisibility(View.INVISIBLE);
                    correctButtonUnhighlighted.setVisibility(View.INVISIBLE);
                    cardText.setText(cards.get(currentIndex).getQuestion());

                    setCardLvl(cards.get(currentIndex).getLevel(),lvl1,lvl2,lvl3,lvl4);
                }
                else {
                    Toast.makeText(getActivity(), "This is the last flashcard in the deck!", Toast.LENGTH_SHORT).show();
                }
            }
        });

        cardBackground.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (tapToViewText.isShown()) {
                    tapToViewText.setVisibility(View.INVISIBLE);
                    correctIncorrectText.setVisibility(View.VISIBLE);
                    incorrectButtonHighlighted.setVisibility(View.INVISIBLE);
                    incorrectButtonUnhighlighted.setVisibility(View.VISIBLE);
                    correctButtonHighlighted.setVisibility(View.INVISIBLE);
                    correctButtonUnhighlighted.setVisibility(View.VISIBLE);
                    cardText.setText(cards.get(currentIndex).getAnswer());
                }
                else {
                    tapToViewText.setVisibility(View.VISIBLE);
                    correctIncorrectText.setVisibility(View.INVISIBLE);
                    incorrectButtonHighlighted.setVisibility(View.INVISIBLE);
                    incorrectButtonUnhighlighted.setVisibility(View.INVISIBLE);
                    correctButtonHighlighted.setVisibility(View.INVISIBLE);
                    correctButtonUnhighlighted.setVisibility(View.INVISIBLE);
                    cardText.setText(cards.get(currentIndex).getQuestion());
                }
            }
        });

        cardText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (tapToViewText.isShown()) {
                    tapToViewText.setVisibility(View.INVISIBLE);
                    correctIncorrectText.setVisibility(View.VISIBLE);
                    incorrectButtonHighlighted.setVisibility(View.INVISIBLE);
                    incorrectButtonUnhighlighted.setVisibility(View.VISIBLE);
                    correctButtonHighlighted.setVisibility(View.INVISIBLE);
                    correctButtonUnhighlighted.setVisibility(View.VISIBLE);
                    cardText.setText(cards.get(currentIndex).getAnswer());
                }
                else {
                    tapToViewText.setVisibility(View.VISIBLE);
                    correctIncorrectText.setVisibility(View.INVISIBLE);
                    incorrectButtonHighlighted.setVisibility(View.INVISIBLE);
                    incorrectButtonUnhighlighted.setVisibility(View.INVISIBLE);
                    correctButtonHighlighted.setVisibility(View.INVISIBLE);
                    correctButtonUnhighlighted.setVisibility(View.INVISIBLE);
                    cardText.setText(cards.get(currentIndex).getQuestion());
                }
            }
        });

        incorrectButtonUnhighlighted.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                incorrectButtonUnhighlighted.setVisibility(View.INVISIBLE);
                incorrectButtonHighlighted.setVisibility(View.VISIBLE);
                previousButton.setEnabled(false);
                nextButton.setEnabled(false);
                cardBackground.setEnabled(false);
                cardText.setEnabled(false);
                incorrectButtonHighlighted.setEnabled(false);
                incorrectButtonUnhighlighted.setEnabled(false);
                correctButtonHighlighted.setEnabled(false);
                correctButtonUnhighlighted.setEnabled(false);
                cards.get(currentIndex).removeLevel();
                cards.get(currentIndex).updateLevels();
                setCardLvl(cards.get(currentIndex).getLevel(),lvl1,lvl2,lvl3,lvl4);
                Toast.makeText(getActivity(), "Better luck next time!", Toast.LENGTH_SHORT).show();
                new CountDownTimer(1800,1000) {
                    @Override
                    public void onTick(long millisUntilFinished) {
                    }

                    @Override
                    public void onFinish() {
                        if (currentIndex < cards.size()-1) {
                            currentIndex += 1;
                            setCardLvl(cards.get(currentIndex).getLevel(),lvl1,lvl2,lvl3,lvl4);
                            previousButton.setEnabled(true);
                            nextButton.setEnabled(true);
                            cardBackground.setEnabled(true);
                            cardText.setEnabled(true);
                            incorrectButtonHighlighted.setEnabled(true);
                            incorrectButtonUnhighlighted.setEnabled(true);
                            correctButtonHighlighted.setEnabled(true);
                            correctButtonUnhighlighted.setEnabled(true);
                            tapToViewText.setVisibility(View.VISIBLE);
                            correctIncorrectText.setVisibility(View.INVISIBLE);
                            incorrectButtonHighlighted.setVisibility(View.INVISIBLE);
                            incorrectButtonUnhighlighted.setVisibility(View.INVISIBLE);
                            correctButtonHighlighted.setVisibility(View.INVISIBLE);
                            correctButtonUnhighlighted.setVisibility(View.INVISIBLE);
                            cardText.setText(cards.get(currentIndex).getQuestion());
                        }
                        else {
                            ((MainActivity)getActivity()).open_screen(new FCEndFragment());
                        }
                    }
                }.start();
            }
        });

        correctButtonUnhighlighted.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                correctButtonUnhighlighted.setVisibility(View.INVISIBLE);
                correctButtonHighlighted.setVisibility(View.VISIBLE);
                previousButton.setEnabled(false);
                nextButton.setEnabled(false);
                cardBackground.setEnabled(false);
                cardText.setEnabled(false);
                incorrectButtonHighlighted.setEnabled(false);
                incorrectButtonUnhighlighted.setEnabled(false);
                correctButtonHighlighted.setEnabled(false);
                correctButtonUnhighlighted.setEnabled(false);
                cards.get(currentIndex).addLevel();
                cards.get(currentIndex).updateLevels();
                setCardLvl(cards.get(currentIndex).getLevel(),lvl1,lvl2,lvl3,lvl4);
                Toast.makeText(getActivity(), "Congratulations!", Toast.LENGTH_SHORT).show();
                new CountDownTimer(1800,1000) {
                    @Override
                    public void onTick(long millisUntilFinished) {

                    }

                    @Override
                    public void onFinish() {
                        if (currentIndex < cards.size()-1) {
                            currentIndex += 1;
                            setCardLvl(cards.get(currentIndex).getLevel(),lvl1,lvl2,lvl3,lvl4);
                            previousButton.setEnabled(true);
                            nextButton.setEnabled(true);
                            cardBackground.setEnabled(true);
                            cardText.setEnabled(true);
                            incorrectButtonHighlighted.setEnabled(true);
                            incorrectButtonUnhighlighted.setEnabled(true);
                            correctButtonHighlighted.setEnabled(true);
                            correctButtonUnhighlighted.setEnabled(true);
                            tapToViewText.setVisibility(View.VISIBLE);
                            correctIncorrectText.setVisibility(View.INVISIBLE);
                            incorrectButtonHighlighted.setVisibility(View.INVISIBLE);
                            incorrectButtonUnhighlighted.setVisibility(View.INVISIBLE);
                            correctButtonHighlighted.setVisibility(View.INVISIBLE);
                            correctButtonUnhighlighted.setVisibility(View.INVISIBLE);
                            cardText.setText(cards.get(currentIndex).getQuestion());
                        }
                        else {
                            ((MainActivity)getActivity()).open_screen(new FCEndFragment());
                        }
                    }
                }.start();
            }
        });

        return view;
    }

    private void setCardLvl(int lvl, ImageView l1, ImageView l2, ImageView l3, ImageView l4) {
        switch(lvl){
            case 1:
                l1.setVisibility(View.VISIBLE);
                l2.setVisibility(View.INVISIBLE);
                l3.setVisibility(View.INVISIBLE);
                l4.setVisibility(View.INVISIBLE);
                break;
            case 2:
                l1.setVisibility(View.INVISIBLE);
                l2.setVisibility(View.VISIBLE);
                l3.setVisibility(View.INVISIBLE);
                l4.setVisibility(View.INVISIBLE);
                break;
            case 3:
                l1.setVisibility(View.INVISIBLE);
                l2.setVisibility(View.INVISIBLE);
                l3.setVisibility(View.VISIBLE);
                l4.setVisibility(View.INVISIBLE);
                break;
            case 4:
                l1.setVisibility(View.INVISIBLE);
                l2.setVisibility(View.INVISIBLE);
                l3.setVisibility(View.INVISIBLE);
                l4.setVisibility(View.VISIBLE);
                break;
        }
    }
}
