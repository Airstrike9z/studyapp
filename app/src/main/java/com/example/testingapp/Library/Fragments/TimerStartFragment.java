package com.example.testingapp.Library.Fragments;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.example.testingapp.MainActivity;
import com.example.testingapp.R;

import java.util.Timer;
import java.util.TimerTask;

public class TimerStartFragment extends Fragment {
    View view;;
    long millisTillDone;
    static CountDownTimer currentCounter;
    String studyID = "study";
    String breakID = "break";
    String currentID;
    MediaPlayer player;

    public TimerStartFragment() { }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.start_timer_screen, container, false);

        TextView studyHrs = view.findViewById(R.id.study_time_hours_text);
        TextView studyMins = view.findViewById(R.id.study_time_minutes_text);
        TextView studySecs = view.findViewById(R.id.study_time_seconds_text);
        TextView breakHrs = view.findViewById(R.id.break_time_hours_text);
        TextView breakMins = view.findViewById(R.id.break_time_minutes_text);
        TextView breakSecs = view.findViewById(R.id.break_time_seconds_text);

        ImageButton pauseButton = view.findViewById(R.id.pause_time_button);
        ImageButton resumeButton = view.findViewById(R.id.pause_time_button_pressed);
        ImageButton skipButtonUnpressed = view.findViewById(R.id.skip_time_button);
        ImageButton skipButtonPressed = view.findViewById(R.id.skip_time_button_pressed);
        ImageButton repeatButtonUnpressed = view.findViewById(R.id.repeat_time_button);
        ImageButton repeatButtonPressed = view.findViewById(R.id.repeat_time_button_pressed);
        ImageButton endSession = view.findViewById(R.id.end_session_button);

        ImageButton libraryButton = view.findViewById(R.id.unflashcards_button);
        ImageButton homePageButton = view.findViewById(R.id.unhome_button);
        ImageButton communityButton = view.findViewById(R.id.uncommunity_button);

        setupTimer(((MainActivity)getActivity()).getTime(),studyHrs,studyMins,studySecs);
        setupTimer(((MainActivity)getActivity()).breakTimeMilli,breakHrs,breakMins,breakSecs);

        startTimer(((MainActivity)getActivity()).getTime(),studyHrs,studyMins,studySecs,studyID);

        libraryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity)getActivity()).hideTimer();
                ((MainActivity)getActivity()).open_screen(new LibraryFragment());
            }
        });

        homePageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity)getActivity()).hideTimer();
                ((MainActivity)getActivity()).open_screen(new HomeFragment());
            }
        });

        communityButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity)getActivity()).hideTimer();
                ((MainActivity)getActivity()).open_screen(new CommunityFragment());
            }
        });

        pauseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pauseButton.setVisibility(View.INVISIBLE);
                resumeButton.setVisibility(View.VISIBLE);
                currentCounter.cancel();
                Toast.makeText(getActivity(), "Timer paused.", Toast.LENGTH_SHORT).show();
            }
        });

        resumeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pauseButton.setVisibility(View.VISIBLE);
                resumeButton.setVisibility(View.INVISIBLE);
                Toast.makeText(getActivity(), "Timer resumed.", Toast.LENGTH_SHORT).show();
                if (currentID.equals(studyID)) {
                    startTimer(millisTillDone,studyHrs,studyMins,studySecs,studyID);
                }
                else {
                    startTimer(millisTillDone,breakHrs,breakMins,breakSecs,breakID);
                }
            }
        });

        skipButtonUnpressed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                skipButtonUnpressed.setVisibility(View.INVISIBLE);
                skipButtonPressed.setVisibility(View.VISIBLE);
                pauseButton.setVisibility(View.VISIBLE);
                resumeButton.setVisibility(View.INVISIBLE);
                currentCounter.cancel();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        skipButtonUnpressed.setVisibility(View.VISIBLE);
                        skipButtonPressed.setVisibility(View.INVISIBLE);
                    }
                },100);
                if (currentID.equals(studyID)) {
                    if (((MainActivity)getActivity()).breakTimeMilli > 0) {
                        setupTimer(0,studyHrs,studyMins,studySecs);
                        startTimer(((MainActivity)getActivity()).breakTimeMilli,breakHrs,breakMins,breakSecs,breakID);
                    }
                    else {
                        ((MainActivity)getActivity()).timerActive = false;
                        Toast.makeText(getActivity(), "Timer skipped! Study session completed.", Toast.LENGTH_SHORT).show();
                        ((MainActivity)getActivity()).open_screen(new TimerPrepFragment());
                    }
                }
                else {
                    ((MainActivity)getActivity()).timerActive = false;
                    Toast.makeText(getActivity(), "Timer skipped! Study session completed.", Toast.LENGTH_SHORT).show();
                    ((MainActivity)getActivity()).open_screen(new TimerPrepFragment());
                }
            }
        });

        repeatButtonUnpressed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                repeatButtonUnpressed.setVisibility(View.INVISIBLE);
                repeatButtonPressed.setVisibility(View.VISIBLE);
                pauseButton.setVisibility(View.VISIBLE);
                resumeButton.setVisibility(View.INVISIBLE);
                currentCounter.cancel();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        repeatButtonUnpressed.setVisibility(View.VISIBLE);
                        repeatButtonPressed.setVisibility(View.INVISIBLE);
                    }
                },100);
                if (currentID.equals(studyID)) {
                    Toast.makeText(getActivity(), "Study timer repeated!", Toast.LENGTH_SHORT).show();
                    startTimer(((MainActivity)getActivity()).getTime(),studyHrs,studyMins,studySecs,studyID);
                }
                else {
                    Toast.makeText(getActivity(), "Break timer repeated!", Toast.LENGTH_SHORT).show();
                    startTimer(((MainActivity)getActivity()).breakTimeMilli,breakHrs,breakMins,breakSecs,breakID);
                }
            }
        });

        endSession.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                currentCounter.cancel();
                ((MainActivity)getActivity()).timerActive = false;
                Toast.makeText(getActivity(), "Study session ended.", Toast.LENGTH_SHORT).show();
                ((MainActivity)getActivity()).open_screen(new TimerPrepFragment());
            }
        });

        return view;
    }

    void startTimer(long mili,TextView hrs, TextView mins, TextView secs, String id) {
        currentID = id;
        if (((MainActivity)getActivity()).timerActive) {
            mili -= System.currentTimeMillis() - ((MainActivity)getActivity()).startTime;
        }
        if (!((MainActivity)getActivity()).timerActive) {
            currentCounter = new CountDownTimer(mili,1000) {
                @Override
                public void onTick(long millisUntilFinished) {
                    long check = millisUntilFinished;
                    millisTillDone = millisUntilFinished;
                    int hour = (int) check / (1000 * 60 * 60);
                    check = (int) (check - (hour * 1000 * 60 * 60));
                    int min = (int) check / (1000 * 60);
                    check = (int) (check - (min * 1000 * 60));
                    int sec = (int) check / (1000);

                    if (hour < 10) {
                        hrs.setText("0" + hour);
                    }
                    else {
                        hrs.setText(String.valueOf(hour));
                    }
                    if (min < 10) {
                        mins.setText("0" + min);
                    }
                    else {
                        mins.setText(String.valueOf(min));
                    }
                    if (sec < 10) {
                        secs.setText("0" + sec);
                    }
                    else {
                        secs.setText(String.valueOf(sec));
                    }
                }
                @Override
                public void onFinish() {
                    player = MediaPlayer.create(getContext(),R.raw.nice_alarm_sound);
                    player.start();
                    Timer timer = new Timer();
                    TimerTask t = new TimerTask() {
                        @Override
                        public void run() {
                            player.pause();
                            player.reset();
                        }
                    };
                    timer.scheduleAtFixedRate(t,5000,1000);
                    if (id.equals(studyID)) {
                        if (((MainActivity)getActivity()).breakTimeMilli > 0) {
                            Toast.makeText(getActivity(), "tmpMsg: studyTimer done. starting breakTimer", Toast.LENGTH_SHORT).show();
                        }
                    }
                    else if (id.equals(breakID)) {
                        Toast.makeText(getActivity(), "Study session completed!", Toast.LENGTH_SHORT).show();
                        ((MainActivity)getActivity()).open_screen(new TimerPrepFragment());
                    }
                }
            };
        }
        else {
            currentCounter = new CountDownTimer(mili,1000) {
                @Override
                public void onTick(long millisUntilFinished) {
                    long check = millisUntilFinished;
                    millisTillDone = millisUntilFinished;
                    int hour = (int) check / (1000 * 60 * 60);
                    check = (int) (check - (hour * 1000 * 60 * 60));
                    int min = (int) check / (1000 * 60);
                    check = (int) (check - (min * 1000 * 60));
                    int sec = (int) check / (1000);

                    if (hour < 10) {
                        hrs.setText("0" + hour);
                    } else {
                        hrs.setText(String.valueOf(hour));
                    }
                    if (min < 10) {
                        mins.setText("0" + min);
                    } else {
                        mins.setText(String.valueOf(min));
                    }
                    if (sec < 10) {
                        secs.setText("0" + sec);
                    } else {
                        secs.setText(String.valueOf(sec));
                    }
                }
                @Override
                public void onFinish() { }
            };
        }

        currentCounter.start();
        if (!((MainActivity)getActivity()).timerActive) {
            ((MainActivity)getActivity()).startTime = System.currentTimeMillis();
        }
        ((MainActivity)getActivity()).timerActive = true;
    }

    void setupTimer(long mili, TextView hrs, TextView mins, TextView secs) {
        long tmpCheck = mili;
        int tmpHour = (int) tmpCheck / (1000 * 60 * 60);
        tmpCheck = (int) (tmpCheck - (tmpHour * 1000 * 60 * 60));
        int tmpMin = (int) tmpCheck / (1000 * 60);
        tmpCheck = (int) (tmpCheck - (tmpMin * 1000 * 60));
        int tmpSec = (int) tmpCheck / (1000);
        if (tmpHour < 10) {
            hrs.setText("0" + tmpHour);
        }
        else {
            hrs.setText(String.valueOf(tmpHour));
        }
        if (tmpMin < 10) {
            mins.setText("0" + tmpMin);
        }
        else {
            mins.setText(String.valueOf(tmpMin));
        }
        if (tmpSec < 10) {
            secs.setText("0" + tmpSec);
        }
        else {
            secs.setText(String.valueOf(tmpSec));
        }
    }
}
