package com.example.testingapp.Library.Fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.example.testingapp.Library.Flashcards.FCCards.FCCardsEditAdapter;
import com.example.testingapp.Library.Flashcards.FCUnit;
import com.example.testingapp.MainActivity;
import com.example.testingapp.R;
import com.example.testingapp.Unique.Flashcard;

import java.util.ArrayList;

public class FCEditFragment extends Fragment {
    View view;
    FCUnit unit;
    ArrayList<Flashcard> cards = new ArrayList<>();
    FCCardsEditAdapter FCCardsEditAdapter;
    RecyclerView fcRecyclerView;

    public FCEditFragment() {
    }

    public FCEditFragment(FCUnit unit, ArrayList<Flashcard> cards) {
        this.unit = unit;
        for (Flashcard card : cards) {
            this.cards.add(new Flashcard(card));
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.edit_fc_cards_screen, container, false);

        ImageButton exit_button = view.findViewById(R.id.exit_button);
        ImageButton confirm_button = view.findViewById(R.id.confirm_button);
        Button new_question_button = view.findViewById(R.id.new_question_button_fc);
        TextView main_title = view.findViewById(R.id.main_title);

        ImageButton homePageButton = view.findViewById(R.id.unhome_button);
        ImageButton timerButton = view.findViewById(R.id.untimer_button);

        homePageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity)getActivity()).open_screen(new HomeFragment());
            }
        });

        timerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((MainActivity)getActivity()).timerActive) {
                    ((MainActivity)getActivity()).showTimer();
                }
                else {
                    ((MainActivity)getActivity()).open_screen(new TimerPrepFragment());
                }
            }
        });

        exit_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager().popBackStack();
            }
        });

        confirm_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FCCardsEditAdapter.confirmEdit(unit);
                getActivity().getSupportFragmentManager().popBackStack();
            }
        });

        new_question_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FCCardsEditAdapter.addData();
            }
        });

        setupRecyclerView();

        return view;
    }

    void setupRecyclerView() {
        fcRecyclerView = view.findViewById(R.id.fc_cards_list_view);
        FCCardsEditAdapter = new FCCardsEditAdapter(getActivity(), this.cards, this);
        FCCardsEditAdapter.setMainActivity(((MainActivity)getActivity()));
        fcRecyclerView.setAdapter(FCCardsEditAdapter); // default starting adapter for FCEditFragment
        ((MainActivity)getActivity()).addAdapter(FCCardsEditAdapter);
    }
}
