package com.example.testingapp.Library.PracticeTests.PTDecks;

import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.testingapp.R;

public class PTDecksViewHolder extends RecyclerView.ViewHolder {
    public PTDecksViewHolder(@NonNull View itemView) {
        super(itemView);
        this.itemView = itemView;
        unitName = itemView.findViewById(R.id.unit_name);
        authorName = itemView.findViewById(R.id.author_name);
        favouriteButton = itemView.findViewById(R.id.favourite_button);
        unfavouriteButton = itemView.findViewById(R.id.unfavourite_button);
        deleteButton = itemView.findViewById(R.id.delete_button);
        fc_unitTint = itemView.findViewById(R.id.decoration3);
        pt_unitTint = itemView.findViewById(R.id.decoration4);
    }

    public View itemView;
    public TextView unitName;
    public TextView authorName;
    public ImageButton deleteButton;
    public ImageButton favouriteButton;
    public ImageButton unfavouriteButton;
    public ImageView fc_unitTint;
    public ImageView pt_unitTint;
}
