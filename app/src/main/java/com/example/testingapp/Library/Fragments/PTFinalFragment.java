package com.example.testingapp.Library.Fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.example.testingapp.Library.PracticeTests.PTFinal.PTFinalViewAdapter;
import com.example.testingapp.MainActivity;
import com.example.testingapp.R;
import com.example.testingapp.Unique.Question;

import java.util.ArrayList;

public class PTFinalFragment extends Fragment {
    View view;
    RecyclerView ptRecyclerView;
    PTFinalViewAdapter PTFinalViewAdapter;
    ArrayList<Question> actualAnswers;
    ArrayList<String> userAnswers;
    String unitName;

    public PTFinalFragment(ArrayList<Question> actualAnswers, ArrayList<String> userAnswers, String unitName) {
        this.unitName = unitName;
        this.actualAnswers = actualAnswers;
        this.userAnswers = userAnswers;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.end_review_pt_study_screen, container, false);

        ImageButton backButton = view.findViewById(R.id.back_button);
        TextView deckName = view.findViewById(R.id.deck_name);
        deckName.setText(unitName);
        ImageButton endReview = view.findViewById(R.id.end_review_button);

        ImageButton homePageButton = view.findViewById(R.id.unhome_button);
        ImageButton timerButton = view.findViewById(R.id.untimer_button);

        homePageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity)getActivity()).open_screen(new HomeFragment());
            }
        });

        timerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((MainActivity)getActivity()).timerActive) {
                    ((MainActivity)getActivity()).showTimer();
                }
                else {
                    ((MainActivity)getActivity()).open_screen(new TimerPrepFragment());
                }
            }
        });

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager().popBackStack();
            }
        });

        endReview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity)getActivity()).open_screen(new PTEndFragment());
            }
        });

        setupRecyclerView();

        return view;
    }

    void setupRecyclerView() {
        ptRecyclerView = view.findViewById(R.id.pt_decks_list_view);
        PTFinalViewAdapter = new PTFinalViewAdapter(getActivity(), actualAnswers, userAnswers, this);
        PTFinalViewAdapter.setMainActivity(((MainActivity)getActivity()));
        ptRecyclerView.setAdapter(PTFinalViewAdapter); // default starting adapter for PTFinalFragment
        ((MainActivity)getActivity()).addAdapter(PTFinalViewAdapter);
    }
}
