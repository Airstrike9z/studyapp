package com.example.testingapp.Library.Fragments;

import android.graphics.drawable.Drawable;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.testingapp.Library.Flashcards.FCDecks.FCDecksEditAdapter;
import com.example.testingapp.Library.Flashcards.FCDecks.FCDecksViewAdapter;
import com.example.testingapp.MainActivity;
import com.example.testingapp.Library.PracticeTests.PTDecks.PTDecksEditAdapter;
import com.example.testingapp.Library.PracticeTests.PTDecks.PTDecksViewAdapter;
import com.example.testingapp.R;
import com.google.firebase.database.DatabaseReference;

public class LibraryFragment extends Fragment {
    View view;
    FCDecksViewAdapter FCDecksViewAdapter;
    FCDecksEditAdapter FCDecksEditAdapter;
    PTDecksViewAdapter PTDecksViewAdapter;
    PTDecksEditAdapter PTDecksEditAdapter;
    RecyclerView decksRecyclerView;

    public LibraryFragment() { }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.library_screen, container, false);

        ImageButton edit_button = view.findViewById(R.id.edit_button);
        ImageButton confirm_button = view.findViewById(R.id.confirm_button);
        ImageButton exit_button = view.findViewById(R.id.exit_button);
        ImageView shape1_fc = view.findViewById(R.id.shape1_fc);
        EditText friend_code_text_fc = view.findViewById(R.id.friend_code_text_fc);
        Button new_set_button_fc = view.findViewById(R.id.new_set_button_fc);
        ImageView shape1_pt = view.findViewById(R.id.shape1_pt);
        EditText friend_code_text_pt = view.findViewById(R.id.friend_code_text_pt);
        Button new_set_button_pt = view.findViewById(R.id.new_set_button_pt);
        TextView practice_tests_tab = view.findViewById(R.id.practice_tests_tab);
        TextView flashcards_tab = view.findViewById(R.id.flashcards_tab);
        TextView mainTitle = view.findViewById(R.id.main_title);
        ImageView fc_line = view.findViewById(R.id.fc_line);
        ImageView pt_line = view.findViewById(R.id.pt_line);
        ImageView backdrop1 = view.findViewById(R.id.backdrop1);
        ImageView backdrop2 = view.findViewById(R.id.backdrop2);
        ImageView backdrop3 = view.findViewById(R.id.backdrop3);
        ImageView backdrop4 = view.findViewById(R.id.backdrop4);

        ImageButton homePageButton = view.findViewById(R.id.unhome_button);
        ImageButton timerButton = view.findViewById(R.id.untimer_button);
        ImageButton communityButton = view.findViewById(R.id.uncommunity_button);

        setupRecyclerView();

        homePageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity)getActivity()).open_screen(new HomeFragment());
            }
        });

        timerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((MainActivity)getActivity()).timerActive) {
                    ((MainActivity)getActivity()).showTimer();
                }
                else {
                    ((MainActivity)getActivity()).open_screen(new TimerPrepFragment());
                }
            }
        });

        communityButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity)getActivity()).open_screen(new CommunityFragment());
            }
        });

        edit_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edit_button.setVisibility(View.INVISIBLE);
                confirm_button.setVisibility(View.VISIBLE);
                exit_button.setVisibility(View.VISIBLE);
                mainTitle.setText("Edit");
                System.out.println("edit button pressed");

                if (decksRecyclerView.getAdapter().equals(PTDecksViewAdapter)) {
                    shape1_fc.setVisibility(View.INVISIBLE);
                    shape1_pt.setVisibility(View.INVISIBLE);
                    friend_code_text_fc.setVisibility(View.INVISIBLE);
                    friend_code_text_pt.setVisibility(View.INVISIBLE);
                    new_set_button_fc.setVisibility(View.INVISIBLE);
                    new_set_button_pt.setVisibility(View.VISIBLE);

                    PTDecksEditAdapter.updateData(PTDecksViewAdapter.getData());
                    decksRecyclerView.setAdapter(PTDecksEditAdapter);
                }
                else if (decksRecyclerView.getAdapter().equals(FCDecksViewAdapter)) {
                    shape1_fc.setVisibility(View.INVISIBLE);
                    shape1_pt.setVisibility(View.INVISIBLE);
                    friend_code_text_fc.setVisibility(View.INVISIBLE);
                    friend_code_text_pt.setVisibility(View.INVISIBLE);
                    new_set_button_fc.setVisibility(View.VISIBLE);
                    new_set_button_pt.setVisibility(View.INVISIBLE);

                    FCDecksEditAdapter.updateData(FCDecksViewAdapter.getData());
                    decksRecyclerView.setAdapter(FCDecksEditAdapter);
                }
            }
        });

        exit_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edit_button.setVisibility(View.VISIBLE);
                confirm_button.setVisibility(View.INVISIBLE);
                exit_button.setVisibility(View.INVISIBLE);
                mainTitle.setText("Library");
                System.out.println("exit button pressed");

                if (decksRecyclerView.getAdapter().equals(PTDecksEditAdapter)) {
                    shape1_fc.setVisibility(View.INVISIBLE);
                    shape1_pt.setVisibility(View.VISIBLE);
                    friend_code_text_fc.setVisibility(View.INVISIBLE);
                    friend_code_text_pt.setVisibility(View.VISIBLE);
                    new_set_button_fc.setVisibility(View.INVISIBLE);
                    new_set_button_pt.setVisibility(View.INVISIBLE);

                    decksRecyclerView.setAdapter(PTDecksViewAdapter);
                }

                else if (decksRecyclerView.getAdapter().equals(FCDecksEditAdapter)) {
                    shape1_fc.setVisibility(View.VISIBLE);
                    shape1_pt.setVisibility(View.INVISIBLE);
                    friend_code_text_fc.setVisibility(View.VISIBLE);
                    friend_code_text_pt.setVisibility(View.INVISIBLE);
                    new_set_button_fc.setVisibility(View.INVISIBLE);
                    new_set_button_pt.setVisibility(View.INVISIBLE);

                    decksRecyclerView.setAdapter(FCDecksViewAdapter);
                }
            }
        });

        confirm_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edit_button.setVisibility(View.VISIBLE);
                confirm_button.setVisibility(View.INVISIBLE);
                exit_button.setVisibility(View.INVISIBLE);
                mainTitle.setText("Library");
                System.out.println("edited changes saved");

                if (decksRecyclerView.getAdapter().equals(PTDecksEditAdapter)) {
                    shape1_fc.setVisibility(View.INVISIBLE);
                    shape1_pt.setVisibility(View.VISIBLE);
                    friend_code_text_fc.setVisibility(View.INVISIBLE);
                    friend_code_text_pt.setVisibility(View.VISIBLE);
                    new_set_button_fc.setVisibility(View.INVISIBLE);
                    new_set_button_pt.setVisibility(View.INVISIBLE);

                    PTDecksEditAdapter.confirmEdit();
                    decksRecyclerView.setAdapter(PTDecksViewAdapter);
                }
                else if (decksRecyclerView.getAdapter().equals(FCDecksEditAdapter)) {
                    shape1_fc.setVisibility(View.VISIBLE);
                    shape1_pt.setVisibility(View.INVISIBLE);
                    friend_code_text_fc.setVisibility(View.VISIBLE);
                    friend_code_text_pt.setVisibility(View.INVISIBLE);
                    new_set_button_fc.setVisibility(View.INVISIBLE);
                    new_set_button_pt.setVisibility(View.INVISIBLE);

                    FCDecksEditAdapter.confirmEdit();
                    decksRecyclerView.setAdapter(FCDecksViewAdapter);
                }
            }
        });

        new_set_button_fc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (decksRecyclerView.getAdapter().equals(PTDecksEditAdapter)) {
                    PTDecksEditAdapter.addData();
                }

                else if (decksRecyclerView.getAdapter().equals(FCDecksEditAdapter)) {
                    FCDecksEditAdapter.addData();
                }
            }
        });

        friend_code_text_fc.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                boolean found = false;
                if (String.valueOf(s).contains("\n")) {
                    if(String.valueOf(s).length() < MainActivity.LENGTHOFCODE+1) {
                        Toast.makeText(getActivity(), "The code is too short!", Toast.LENGTH_SHORT).show();
                    }
                    else {
                        for (DatabaseReference dbRef : ((MainActivity)getActivity()).allFCDbRefs) {
                            String ref = dbRef.getKey().substring(1,MainActivity.LENGTHOFCODE+1).toLowerCase();
                            if (ref.equals(String.valueOf(s).replace("\n","").toLowerCase())) {
                                Toast.makeText(getActivity(), "Deck added!", Toast.LENGTH_SHORT).show();
                                dbRef.child(MainActivity.VIEWER).push().setValue(((MainActivity)getActivity()).owner);
                                friend_code_text_fc.setText("");
                                found = true;
                            }
                        }
                        if (!found) {
                            Toast.makeText(getActivity(), "Cannot find a Flashcard unit with this code.", Toast.LENGTH_SHORT).show();
                        }
                    }

                }
            }
            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        friend_code_text_pt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                boolean found = false;
                if (String.valueOf(s).contains("\n")) {
                    if(String.valueOf(s).length() < MainActivity.LENGTHOFCODE+1) {
                        Toast.makeText(getActivity(), "The code is too short!", Toast.LENGTH_SHORT).show();
                    }
                    else {
                        for (DatabaseReference dbRef : ((MainActivity)getActivity()).allPTDbRefs) {
                            String ref = dbRef.getKey().substring(1,MainActivity.LENGTHOFCODE+1).toLowerCase();
                            if (ref.equals(String.valueOf(s).replace("\n","").toLowerCase())) {
                                dbRef.child(MainActivity.VIEWER).push().setValue(((MainActivity)getActivity()).owner);
                                friend_code_text_fc.setText("");
                                found = true;
                            }
                        }
                        if (!found) {
                            Toast.makeText(getActivity(), "Cannot find a Practice Test unit with this code.", Toast.LENGTH_SHORT).show();
                        }
                    }

                }
            }
            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        new_set_button_pt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (decksRecyclerView.getAdapter().equals(PTDecksEditAdapter)) {
                    PTDecksEditAdapter.addData();
                }

                else if (decksRecyclerView.getAdapter().equals(FCDecksEditAdapter)) {
                    FCDecksEditAdapter.addData();
                }
            }
        });

        practice_tests_tab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Drawable ptLine = getResources().getDrawable(R.drawable.selected_line);
                Drawable fcLine = getResources().getDrawable(R.drawable.unselected_line);
                practice_tests_tab.setTextColor(getResources().getColor(R.color.selectedColor));
                flashcards_tab.setTextColor(getResources().getColor(R.color.unselectedColor));
                pt_line.setBackground(ptLine);
                fc_line.setBackground(fcLine);
                backdrop1.setVisibility(View.INVISIBLE);
                backdrop2.setVisibility(View.VISIBLE);
                backdrop3.setVisibility(View.INVISIBLE);
                backdrop4.setVisibility(View.VISIBLE);

                if (decksRecyclerView.getAdapter().equals(FCDecksViewAdapter)) {
                    shape1_fc.setVisibility(View.INVISIBLE);
                    shape1_pt.setVisibility(View.VISIBLE);
                    friend_code_text_fc.setVisibility(View.INVISIBLE);
                    friend_code_text_pt.setVisibility(View.VISIBLE);
                    new_set_button_fc.setVisibility(View.INVISIBLE);
                    new_set_button_pt.setVisibility(View.INVISIBLE);

                    decksRecyclerView.setAdapter(PTDecksViewAdapter);
                }
                else if (decksRecyclerView.getAdapter().equals(FCDecksEditAdapter)) {
                    shape1_fc.setVisibility(View.INVISIBLE);
                    shape1_pt.setVisibility(View.INVISIBLE);
                    friend_code_text_fc.setVisibility(View.INVISIBLE);
                    friend_code_text_pt.setVisibility(View.INVISIBLE);
                    new_set_button_fc.setVisibility(View.INVISIBLE);
                    new_set_button_pt.setVisibility(View.VISIBLE);

                    decksRecyclerView.setAdapter(PTDecksEditAdapter);
                }
            }
        });

        flashcards_tab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Drawable ptLine = getResources().getDrawable(R.drawable.unselected_line);
                Drawable fcLine = getResources().getDrawable(R.drawable.selected_line);
                practice_tests_tab.setTextColor(getResources().getColor(R.color.unselectedColor));
                flashcards_tab.setTextColor(getResources().getColor(R.color.selectedColor));
                pt_line.setBackground(ptLine);
                fc_line.setBackground(fcLine);
                backdrop1.setVisibility(View.VISIBLE);
                backdrop2.setVisibility(View.INVISIBLE);
                backdrop3.setVisibility(View.VISIBLE);
                backdrop4.setVisibility(View.INVISIBLE);

                if (decksRecyclerView.getAdapter().equals(PTDecksViewAdapter)) {
                    shape1_fc.setVisibility(View.VISIBLE);
                    shape1_pt.setVisibility(View.INVISIBLE);
                    friend_code_text_fc.setVisibility(View.VISIBLE);
                    friend_code_text_pt.setVisibility(View.INVISIBLE);
                    new_set_button_fc.setVisibility(View.INVISIBLE);
                    new_set_button_pt.setVisibility(View.INVISIBLE);

                    decksRecyclerView.setAdapter(FCDecksViewAdapter);
                }
                else if (decksRecyclerView.getAdapter().equals(PTDecksEditAdapter)) {
                    shape1_fc.setVisibility(View.INVISIBLE);
                    shape1_pt.setVisibility(View.INVISIBLE);
                    friend_code_text_fc.setVisibility(View.INVISIBLE);
                    friend_code_text_pt.setVisibility(View.INVISIBLE);
                    new_set_button_fc.setVisibility(View.VISIBLE);
                    new_set_button_pt.setVisibility(View.INVISIBLE);

                    decksRecyclerView.setAdapter(FCDecksEditAdapter);
                }
            }
        });

        return view;
    }

    @Override
    public void onDestroyView() {
        ((MainActivity)getActivity()).removeAdapter(FCDecksViewAdapter);
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
//        ((MainActivity)getActivity()).clearBackStackInclusive(this.getTag());
    }

    void setupRecyclerView() {
        decksRecyclerView = view.findViewById(R.id.fc_decks_list_view);
        FCDecksViewAdapter = new FCDecksViewAdapter(getActivity(), ((MainActivity)getActivity()).fcUnits, this);
        FCDecksEditAdapter = new FCDecksEditAdapter(getActivity(), ((MainActivity)getActivity()).fcUnits, this);
        PTDecksViewAdapter = new PTDecksViewAdapter(getActivity(), ((MainActivity)getActivity()).ptUnits, this);
        PTDecksEditAdapter = new PTDecksEditAdapter(getActivity(), ((MainActivity)getActivity()).ptUnits, this);
        FCDecksViewAdapter.setMainActivity(((MainActivity)getActivity()));
        FCDecksEditAdapter.setMainActivity(((MainActivity)getActivity()));
        PTDecksViewAdapter.setMainActivity(((MainActivity)getActivity()));
        PTDecksEditAdapter.setMainActivity(((MainActivity)getActivity()));
        decksRecyclerView.setAdapter(FCDecksViewAdapter); // default starting adapter for libraryfragment
        ((MainActivity)getActivity()).addAdapter(FCDecksViewAdapter);
        ((MainActivity)getActivity()).addAdapter(FCDecksEditAdapter);
        ((MainActivity)getActivity()).addAdapter(PTDecksViewAdapter);
        ((MainActivity)getActivity()).addAdapter(PTDecksEditAdapter);
    }

}