package com.example.testingapp.Library.PracticeTests.PTCards;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.example.testingapp.Library.Flashcards.FCCards.FCCardsEditHolder;
import com.example.testingapp.Library.Flashcards.FCUnit;
import com.example.testingapp.Library.PracticeTests.PTUnit;
import com.example.testingapp.MainActivity;
import com.example.testingapp.R;
import com.example.testingapp.Unique.Flashcard;
import com.example.testingapp.Unique.Question;

import java.util.ArrayList;

public class PTCardsEditAdapter extends RecyclerView.Adapter<PTCardsEditHolder>{

    private Context context;
    private ArrayList<Question> questions = new ArrayList<>();
    private ArrayList<Question> questionsToRemove = new ArrayList<>();
    MainActivity mainActivity;
    Fragment fragment;

    public PTCardsEditAdapter(@NonNull Context context, @NonNull ArrayList<Question> questions, @NonNull Fragment fragment) {
        this.context = context;
        this.questions.addAll(questions);
        this.fragment = fragment;
    }

    @NonNull
    @Override
    public PTCardsEditHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View itemView = inflater.inflate(R.layout.item_edit_card, parent, false); // replace with item_edit_practicetest or whatever itll be called
        PTCardsEditHolder viewHolder = new PTCardsEditHolder(itemView);
        return viewHolder;
    }

    public void setMainActivity(MainActivity mainActivity) {
        this.mainActivity = mainActivity;
    }

    @Override
    public void onBindViewHolder(@NonNull PTCardsEditHolder holder, int position) {
        Question question = questions.get(position);
        holder.cardQuestion.setText(question.getQuestion());
        holder.cardAnswer.setText(question.getAnswer());

        holder.cardQuestion.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                question.setQuestion(s.toString());
            }
        });

        holder.cardAnswer.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                question.setAnswer(s.toString());
            }
        });

        holder.duplicateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Question newQuestion = new Question();
                newQuestion.setQuestion(question.getQuestion());
                newQuestion.setAnswer(question.getAnswer());
                questions.add(newQuestion);
                notifyDataSetChanged();
            }
        });

        holder.deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                questionsToRemove.add(question);
                questions.remove(question);
                notifyDataSetChanged();
            }
        });
    }

    @Override
    public int getItemCount() {
        return questions.size();
    }

    public ArrayList<Question> getData() {
        return questions;
    }

    public void updateData(ArrayList<Question> questions) {
        this.questions.clear();
        for (Question question : questions) {
            this.questions.add(question);
        }
        notifyDataSetChanged();
    }

    public void confirmEdit(PTUnit unit) {
        for (Question question : questionsToRemove) {
            question.remove();
        }
        for (Question question : this.questions) {
            if (question.getRef() == null) {
                mainActivity.addQuestion(unit,question);
            }
            else {
                question.update();
            }
        }
    }

    public void addData() {
        Question question = new Question();
        question.setQuestion("New question");// currently set to none. change to editable name later
        question.setAnswer("New answer"); // also change to editable name later
        this.questions.add(question);
        notifyDataSetChanged();
    }

}
