package com.example.testingapp.Library.Flashcards.FCCards;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.example.testingapp.MainActivity;
import com.example.testingapp.R;
import com.example.testingapp.Unique.Flashcard;

import java.util.ArrayList;

public class FCCardsViewAdapter extends RecyclerView.Adapter<FCCardsViewHolder>{

    private Context context;
    private ArrayList<Flashcard> flashcards;
    MainActivity mainActivity;
    Fragment fragment;

    public FCCardsViewAdapter(@NonNull Context context, @NonNull ArrayList<Flashcard> flashcards, @NonNull Fragment fragment) {
        this.context = context;
        this.flashcards = flashcards;
        this.fragment = fragment;
    }

    @NonNull
    @Override
    public FCCardsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View itemView = inflater.inflate(R.layout.item_card, parent, false);
        FCCardsViewHolder viewHolder = new FCCardsViewHolder(itemView);
        return viewHolder;
    }

    public void setMainActivity(MainActivity mainActivity) {
        this.mainActivity = mainActivity;
    }

    @Override
    public void onBindViewHolder(@NonNull FCCardsViewHolder holder, int position) {
        Flashcard card = flashcards.get(position);
        holder.questionText.setText(card.getQuestion());
        holder.answerText.setText(card.getAnswer());

        switch (card.getLevel()) {
            case 1:
                holder.prof1.setVisibility(View.VISIBLE);
                holder.prof2.setVisibility(View.INVISIBLE);
                holder.prof3.setVisibility(View.INVISIBLE);
                holder.prof4.setVisibility(View.INVISIBLE);
                break;
            case 2:
                holder.prof1.setVisibility(View.INVISIBLE);
                holder.prof2.setVisibility(View.VISIBLE);
                holder.prof3.setVisibility(View.INVISIBLE);
                holder.prof4.setVisibility(View.INVISIBLE);
                break;
            case 3:
                holder.prof1.setVisibility(View.INVISIBLE);
                holder.prof2.setVisibility(View.INVISIBLE);
                holder.prof3.setVisibility(View.VISIBLE);
                holder.prof4.setVisibility(View.INVISIBLE);
                break;
            case 4:
                holder.prof1.setVisibility(View.INVISIBLE);
                holder.prof2.setVisibility(View.INVISIBLE);
                holder.prof3.setVisibility(View.INVISIBLE);
                holder.prof4.setVisibility(View.VISIBLE);
                break;
        }
    }

    @Override
    public int getItemCount() {
        return flashcards.size();
    }

    public ArrayList<Flashcard> getData() {
        return flashcards;
    }

}
