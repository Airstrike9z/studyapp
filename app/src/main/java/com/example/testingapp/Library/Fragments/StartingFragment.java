package com.example.testingapp.Library.Fragments;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;

import androidx.fragment.app.Fragment;

import com.example.testingapp.MainActivity;
import com.example.testingapp.R;

public class StartingFragment extends Fragment {
    View view;

    public StartingFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.starting_screen, container, false);

        EditText username = view.findViewById(R.id.username_entry);
        Button startConfirmed = view.findViewById(R.id.enter_button_confirm);
        Button startUnconfirmed = view.findViewById(R.id.enter_button_unconfirm);
        startConfirmed.setVisibility(View.INVISIBLE);
        startUnconfirmed.setVisibility(View.VISIBLE);

        startConfirmed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity)getActivity()).owner = username.getText().toString();
                ((MainActivity)getActivity()).startDatabase();
                ((MainActivity)getActivity()).open_screen(new HomeFragment());
            }
        });

        username.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (String.valueOf(s).contains("\n")) {
                    ((MainActivity)getActivity()).owner = String.valueOf(s).replace("\n","");
                    ((MainActivity)getActivity()).startDatabase();
                    ((MainActivity)getActivity()).open_screen(new LibraryFragment());
                }
                if (!username.getText().toString().isEmpty()) {
                    startConfirmed.setVisibility(View.VISIBLE);
                    startUnconfirmed.setVisibility(View.INVISIBLE);
                }
                else {
                    startConfirmed.setVisibility(View.INVISIBLE);
                    startUnconfirmed.setVisibility(View.VISIBLE);
                }
            }
        });

        return view;
    }
}
