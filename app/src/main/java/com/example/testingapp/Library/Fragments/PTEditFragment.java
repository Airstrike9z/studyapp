package com.example.testingapp.Library.Fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.example.testingapp.Library.PracticeTests.PTCards.PTCardsEditAdapter;
import com.example.testingapp.Library.PracticeTests.PTUnit;
import com.example.testingapp.MainActivity;
import com.example.testingapp.R;
import com.example.testingapp.Unique.Question;

import java.util.ArrayList;

public class PTEditFragment extends Fragment {
    View view;
    PTUnit unit;
    ArrayList<Question> questions = new ArrayList<>();
    PTCardsEditAdapter PTCardsEditAdapter;
    RecyclerView ptRecyclerView;

    public PTEditFragment() {
    }

    public PTEditFragment(PTUnit unit, ArrayList<Question> questions) {
        this.unit = unit;
        for (Question question : questions) {
            this.questions.add(new Question(question));
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.edit_pt_cards_screen, container, false);

        ImageButton exit_button = view.findViewById(R.id.exit_button);
        ImageButton confirm_button = view.findViewById(R.id.confirm_button);
        Button new_question_button = view.findViewById(R.id.new_question_button_pt);
        TextView main_title = view.findViewById(R.id.main_title);

        ImageButton homePageButton = view.findViewById(R.id.unhome_button);
        ImageButton timerButton = view.findViewById(R.id.untimer_button);

        homePageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity)getActivity()).open_screen(new HomeFragment());
            }
        });

        timerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((MainActivity)getActivity()).timerActive) {
                    ((MainActivity)getActivity()).showTimer();
                }
                else {
                    ((MainActivity)getActivity()).open_screen(new TimerPrepFragment());
                }
            }
        });

        exit_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager().popBackStack();
            }
        });

        confirm_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PTCardsEditAdapter.confirmEdit(unit);
                getActivity().getSupportFragmentManager().popBackStack();
            }
        });

        new_question_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PTCardsEditAdapter.addData();
            }
        });

        setupRecyclerView();

        return view;
    }

    void setupRecyclerView() {
        ptRecyclerView = view.findViewById(R.id.pt_cards_list_view);
        PTCardsEditAdapter = new PTCardsEditAdapter(getActivity(), this.questions, this);
        PTCardsEditAdapter.setMainActivity(((MainActivity)getActivity()));
        ptRecyclerView.setAdapter(PTCardsEditAdapter); // default starting adapter for PTEditFragment
        ((MainActivity)getActivity()).addAdapter(PTCardsEditAdapter);
    }
}
