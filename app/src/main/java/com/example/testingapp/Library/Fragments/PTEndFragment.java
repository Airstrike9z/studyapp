package com.example.testingapp.Library.Fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import androidx.fragment.app.Fragment;

import com.example.testingapp.MainActivity;
import com.example.testingapp.R;

public class PTEndFragment extends Fragment {
    View view;

    public PTEndFragment() { }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.finished_pt_study_screen, container, false);

        ImageButton doneButton = view.findViewById(R.id.done_button);

        ImageButton homePageButton = view.findViewById(R.id.unhome_button);
        ImageButton timerButton = view.findViewById(R.id.untimer_button);

        homePageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity)getActivity()).open_screen(new HomeFragment());
            }
        });

        timerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((MainActivity)getActivity()).timerActive) {
                    ((MainActivity)getActivity()).showTimer();
                }
                else {
                    ((MainActivity)getActivity()).open_screen(new TimerPrepFragment());
                }
            }
        });

        doneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity)getActivity()).open_screen(new LibraryFragment());
            }
        });

        return view;
    }
}
