package com.example.testingapp.Library.Homepage.Favourites;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.example.testingapp.Library.Fragments.FCViewFragment;
import com.example.testingapp.Library.Fragments.HomeFragment;
import com.example.testingapp.Library.Fragments.PTViewFragment;
import com.example.testingapp.MainActivity;
import com.example.testingapp.R;

import com.example.testingapp.Library.Flashcards.FCUnit;
import com.example.testingapp.Library.PracticeTests.PTUnit;

import java.util.ArrayList;

public class FavouritesViewAdapter extends RecyclerView.Adapter<FavouritesViewHolder>{

    Context context;
    ArrayList<Object> favourites;
    MainActivity mainActivity;
    Fragment fragment;
    private static final String fcDeck = "Flashcard Deck";
    private static final String ptTest = "Practice Test";

    public FavouritesViewAdapter(@NonNull Context context, @NonNull ArrayList<Object> favourites, @NonNull Fragment fragment) {
        this.context = context;
        this.favourites = favourites;
        this.fragment = fragment;
    }

    @NonNull
    @Override
    public FavouritesViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View itemView = inflater.inflate(R.layout.item_saved, parent, false);
        FavouritesViewHolder viewHolder = new FavouritesViewHolder(itemView);
        return viewHolder;
    }

    public void setMainActivity(MainActivity mainActivity) {
        this.mainActivity = mainActivity;
    }

    @Override
    public void onBindViewHolder(@NonNull FavouritesViewHolder holder, int position) {
        holder.favouriteButton.setVisibility(View.VISIBLE);
        holder.unfavouriteButton.setVisibility(View.INVISIBLE);

        if (favourites.get(position).getClass().equals(FCUnit.class)) {
            FCUnit unit = FCUnit.class.cast(favourites.get(position));
            holder.unitName.setText(unit.getUnitName());
            holder.fcTint.setVisibility(View.VISIBLE);
            holder.ptTint.setVisibility(View.INVISIBLE);
            holder.deckType.setText(fcDeck);

            holder.favouriteButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    favourites.remove(unit);
                    unit.removeFavoriteUser(mainActivity.owner);
                    unit.updateFavorites();
                    notifyDataSetChanged();
                    mainActivity.getSupportFragmentManager().popBackStack();
                    mainActivity.open_screen(new HomeFragment());
                }
            });

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mainActivity.setCurrentFCUnit(unit);
                    mainActivity.open_screen(new FCViewFragment());
                }
            });
        }
        else if (favourites.get(position).getClass().equals(PTUnit.class)) {
            PTUnit unit = PTUnit.class.cast(favourites.get(position));
            holder.unitName.setText(unit.getUnitName());
            holder.fcTint.setVisibility(View.INVISIBLE);
            holder.ptTint.setVisibility(View.VISIBLE);
            holder.deckType.setText(ptTest);

            holder.favouriteButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    favourites.remove(unit);
                    unit.removeFavoriteUser(mainActivity.owner);
                    unit.updateFavorites();
                    notifyDataSetChanged();
                    mainActivity.getSupportFragmentManager().popBackStack();
                    mainActivity.open_screen(new HomeFragment());
                }
            });

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mainActivity.setCurrentPTUnit(unit);
                    mainActivity.open_screen(new PTViewFragment());
                }
            });
        }


    }

    @Override
    public int getItemCount() {
        return favourites.size();
    }
}
