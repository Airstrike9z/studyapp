package com.example.testingapp.Library.PracticeTests.PTCards;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.example.testingapp.MainActivity;
import com.example.testingapp.R;
import com.example.testingapp.Unique.Flashcard;
import com.example.testingapp.Unique.Question;

import java.util.ArrayList;

public class PTCardsViewAdapter extends RecyclerView.Adapter<PTCardsViewHolder>{

    private Context context;
    private ArrayList<Question> questions;
    MainActivity mainActivity;
    Fragment fragment;

    public PTCardsViewAdapter(@NonNull Context context, @NonNull ArrayList<Question> questions, @NonNull Fragment fragment) {
        this.context = context;
        this.questions = questions;
        this.fragment = fragment;
    }

    @NonNull
    @Override
    public PTCardsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View itemView = inflater.inflate(R.layout.item_card, parent, false);
        PTCardsViewHolder viewHolder = new PTCardsViewHolder(itemView);
        return viewHolder;
    }

    public void setMainActivity(MainActivity mainActivity) {
        this.mainActivity = mainActivity;
    }

    @Override
    public void onBindViewHolder(@NonNull PTCardsViewHolder holder, int position) {
        Question question = questions.get(position);
        holder.questionText.setText(question.getQuestion());
        holder.answerText.setText(question.getAnswer());
    }

    @Override
    public int getItemCount() {
        return questions.size();
    }

    public ArrayList<Question> getData() {
        return questions;
    }

}
