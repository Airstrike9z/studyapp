package com.example.testingapp.Library.Flashcards.FCDecks;

import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.testingapp.R;

public class FCDecksEditHolder extends RecyclerView.ViewHolder {
    public FCDecksEditHolder(@NonNull View itemView) {
        super(itemView);
        this.itemView = itemView;
        unitName = itemView.findViewById(R.id.unit_name);
        authorName = itemView.findViewById(R.id.author_name);
        favouriteButton = itemView.findViewById(R.id.favourite_button);
        unfavouriteButton = itemView.findViewById(R.id.unfavourite_button);
        deleteButton = itemView.findViewById(R.id.delete_button);
    }

    public View itemView;
    public EditText unitName;
    public TextView authorName;
    public ImageButton deleteButton;
    public ImageButton favouriteButton;
    public ImageButton unfavouriteButton;
}