package com.example.testingapp.Library.Fragments;

import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.example.testingapp.MainActivity;
import com.example.testingapp.R;

public class TimerPrepFragment extends Fragment {
    View view;

    public TimerPrepFragment() { }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.prepare_timer_screen, container, false);

        ImageButton startButtonUnclicked = view.findViewById(R.id.start_timer_button);
        ImageButton startButtonClicked = view.findViewById(R.id.started_timer_button);

        EditText studyTimeHours = view.findViewById(R.id.study_time_hour_text);
        EditText studyTimeMinutes = view.findViewById(R.id.study_time_minute_text);
        EditText studyTimeSeconds = view.findViewById(R.id.study_time_second_text);

        EditText breakTimeHours = view.findViewById(R.id.break_time_hour_text);
        EditText breakTimeMinutes = view.findViewById(R.id.break_time_minute_text);
        EditText breakTimeSeconds = view.findViewById(R.id.break_time_second_text);

        ImageButton libraryButton = view.findViewById(R.id.unflashcards_button);
        ImageButton homePageButton = view.findViewById(R.id.unhome_button);
        ImageButton communityButton = view.findViewById(R.id.uncommunity_button);

        libraryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity)getActivity()).open_screen(new LibraryFragment());
            }
        });

        homePageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity)getActivity()).open_screen(new HomeFragment());
            }
        });

        communityButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity)getActivity()).open_screen(new CommunityFragment());
            }
        });

        startButtonUnclicked.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String sthString = studyTimeHours.getText().toString();
                String stmString = studyTimeMinutes.getText().toString();
                String stsString = studyTimeSeconds.getText().toString();
                String bthString = breakTimeHours.getText().toString();
                String btmString = breakTimeMinutes.getText().toString();
                String btsString = breakTimeSeconds.getText().toString();

                String studyTime = sthString + stmString + stsString;
                if (studyTime.length() == 0) {
                    Toast.makeText(getActivity(), "Please input a study time!", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (sthString.length() == 0) {
                    studyTimeHours.setText("00");
                }
                if (stmString.length() == 0) {
                    studyTimeMinutes.setText("00");
                }
                if (stsString.length() == 0) {
                    studyTimeSeconds.setText("00");
                }
                if (bthString.length() == 0) {
                    breakTimeHours.setText("00");
                }
                if (btmString.length() == 0) {
                    breakTimeMinutes.setText("00");
                }
                if (btsString.length() == 0) {
                    breakTimeSeconds.setText("00");
                }

                long breakTimeMilli = (Long.parseLong(breakTimeHours.getText().toString()) * 60 * 60000) + (Long.parseLong(breakTimeMinutes.getText().toString()) * 60000) + (Long.parseLong(breakTimeSeconds.getText().toString()) * 1000);
                long studyTimeMilli = (Long.parseLong(studyTimeHours.getText().toString()) * 60 * 60000) + (Long.parseLong(studyTimeMinutes.getText().toString()) * 60000) + (Long.parseLong(studyTimeSeconds.getText().toString()) * 1000);

                if (studyTimeMilli == 0) {
                    Toast.makeText(getActivity(), "Study time must be at least 1 second!", Toast.LENGTH_SHORT).show();
                    return;
                }

//                ((MainActivity)getActivity()).studyTimeMilli = studyTimeMilli;
                ((MainActivity)getActivity()).setTime(studyTimeMilli);
                ((MainActivity)getActivity()).breakTimeMilli = breakTimeMilli;

                startButtonClicked.setVisibility(View.VISIBLE);
                startButtonUnclicked.setVisibility(View.INVISIBLE);

                Toast.makeText(getActivity(), "Timer started!", Toast.LENGTH_SHORT).show();
                new CountDownTimer(500,500) {
                    @Override
                    public void onTick(long millisUntilFinished) {
                    }

                    @Override
                    public void onFinish() {
                        ((MainActivity)getActivity()).open_screen(new TimerStartFragment(),"TIMERSTARTED");
                    }
                }.start();
            }
        });

        return view;
    }
}
