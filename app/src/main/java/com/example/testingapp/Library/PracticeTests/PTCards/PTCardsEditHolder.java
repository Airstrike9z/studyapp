package com.example.testingapp.Library.PracticeTests.PTCards;

import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.testingapp.R;

public class PTCardsEditHolder extends RecyclerView.ViewHolder {
    public PTCardsEditHolder(@NonNull View itemView) {
        super(itemView);
        this.itemView = itemView;
        cardQuestion = itemView.findViewById(R.id.card_question);
        cardAnswer = itemView.findViewById(R.id.card_answer);
        deleteButton = itemView.findViewById(R.id.delete_button);
        duplicateButton = itemView.findViewById(R.id.duplicate_button);
    }

    public View itemView;
    public EditText cardQuestion;
    public EditText cardAnswer;
    public ImageButton deleteButton;
    public ImageButton duplicateButton;
}
