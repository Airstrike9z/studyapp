package com.example.testingapp.Library.PracticeTests.PTDecks;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.example.testingapp.Library.Fragments.PTViewFragment;
import com.example.testingapp.Library.PracticeTests.PTUnit;
import com.example.testingapp.MainActivity;
import com.example.testingapp.R;

import java.util.ArrayList;

public class PTDecksViewAdapter extends RecyclerView.Adapter<PTDecksViewHolder> {

    Context context;
    ArrayList<PTUnit> PTUnits;
    MainActivity mainActivity;
    Fragment fragment;

    public PTDecksViewAdapter(@NonNull Context context, @NonNull ArrayList<PTUnit> units, @NonNull Fragment fragment) {
        this.context = context;
        this.PTUnits = units;
        this.fragment = fragment;
    }

    @NonNull
    @Override
    public PTDecksViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View itemView = inflater.inflate(R.layout.item_unit, parent, false);
        PTDecksViewHolder viewHolder = new PTDecksViewHolder(itemView);
        return viewHolder;
    }

    public void setMainActivity(MainActivity mainActivity) {
        this.mainActivity = mainActivity;
    }

    @Override
    public void onBindViewHolder(@NonNull PTDecksViewHolder holder, int position) {
        PTUnit unit = PTUnits.get(position);
        holder.unitName.setText(unit.getUnitName());
        holder.authorName.setText(unit.getAuthorName());
        holder.deleteButton.setVisibility(View.INVISIBLE);
        holder.fc_unitTint.setVisibility(View.INVISIBLE);
        holder.pt_unitTint.setVisibility(View.VISIBLE);
        if (unit.getFavoriteUsers().contains(mainActivity.owner)) {
            holder.favouriteButton.setVisibility(View.VISIBLE);
            holder.unfavouriteButton.setVisibility(View.INVISIBLE);
        }
        else {
            holder.favouriteButton.setVisibility(View.INVISIBLE);
            holder.unfavouriteButton.setVisibility(View.VISIBLE);
        }

        holder.favouriteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.favouriteButton.setVisibility(View.INVISIBLE);
                holder.unfavouriteButton.setVisibility(View.VISIBLE);
                unit.removeFavoriteUser(mainActivity.owner);
                unit.updateFavorites();
            }
        });

        holder.unfavouriteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.favouriteButton.setVisibility(View.VISIBLE);
                holder.unfavouriteButton.setVisibility(View.INVISIBLE);
                unit.addFavoriteUser(mainActivity.owner);
                unit.updateFavorites();
            }
        });

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mainActivity.setCurrentPTUnit(unit);
                mainActivity.open_screen(new PTViewFragment());
            }
        });
    }

    @Override
    public int getItemCount() {
        return PTUnits.size();
    }

    public ArrayList<PTUnit> getData() {
        return PTUnits;
    }
}
