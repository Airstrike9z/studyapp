package com.example.testingapp.Library.Fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.example.testingapp.Library.Homepage.Favourites.FavouritesViewAdapter;
import com.example.testingapp.MainActivity;
import com.example.testingapp.R;

public class HomeFragment extends Fragment {
    View view;
    RecyclerView favouritesRecyclerView;
    FavouritesViewAdapter FavouritesViewAdapter;

    public HomeFragment() { }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.homepage_screen, container, false);

        TextView username = view.findViewById(R.id.username);
        username.setText(((MainActivity)getActivity()).owner);
        TextView emptyView = view.findViewById(R.id.empty_list_text);

        ImageButton libraryButton = view.findViewById(R.id.unflashcards_button);
        ImageButton timerButton = view.findViewById(R.id.untimer_button);
        ImageButton communityButton = view.findViewById(R.id.uncommunity_button);

        libraryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity)getActivity()).open_screen(new LibraryFragment());
            }
        });

        timerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((MainActivity)getActivity()).timerActive) {
                    ((MainActivity)getActivity()).showTimer();
                }
                else {
                    ((MainActivity)getActivity()).open_screen(new TimerPrepFragment());
                }
            }
        });

        communityButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity)getActivity()).open_screen(new CommunityFragment());
            }
        });

        setupRecyclerView();

//        if (((MainActivity)getActivity()).favourites.size() == 0) {
//            favouritesRecyclerView.setVisibility(View.GONE);
//            emptyView.setVisibility(View.VISIBLE);
//        }
//        else {
            favouritesRecyclerView.setVisibility(View.VISIBLE);
            emptyView.setVisibility(View.GONE);
//        }
        favouritesRecyclerView.setAdapter(FavouritesViewAdapter);

        return view;
    }

    void setupRecyclerView() {
        favouritesRecyclerView = view.findViewById(R.id.saved_list_view);
        FavouritesViewAdapter = new FavouritesViewAdapter(getActivity(), ((MainActivity)getActivity()).favourites, this);
        FavouritesViewAdapter.setMainActivity(((MainActivity)getActivity()));
        favouritesRecyclerView.setAdapter(FavouritesViewAdapter);
        ((MainActivity)getActivity()).addAdapter(FavouritesViewAdapter);
    }
}
