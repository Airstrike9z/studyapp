package com.example.testingapp.Library.Flashcards.FCDecks;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.example.testingapp.Library.Flashcards.FCUnit;
import com.example.testingapp.Library.Fragments.FCViewFragment;
import com.example.testingapp.MainActivity;
import com.example.testingapp.R;

import java.util.ArrayList;

public class FCDecksViewAdapter extends RecyclerView.Adapter<FCDecksViewHolder> {

    Context context;
    ArrayList<FCUnit> FCUnits;
    MainActivity mainActivity;
    Fragment fragment;

    public FCDecksViewAdapter(@NonNull Context context, @NonNull ArrayList<FCUnit> units, @NonNull Fragment fragment) {
        this.context = context;
        this.FCUnits = units;
        this.fragment = fragment;
    }

    @NonNull
    @Override
    public FCDecksViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View itemView = inflater.inflate(R.layout.item_unit, parent, false);
        FCDecksViewHolder viewHolder = new FCDecksViewHolder(itemView);
        return viewHolder;
    }

    public void setMainActivity(MainActivity mainActivity) {
        this.mainActivity = mainActivity;
    }

    @Override
    public void onBindViewHolder(@NonNull FCDecksViewHolder holder, int position) {
        FCUnit unit = FCUnits.get(position);
        holder.unitName.setText(unit.getUnitName());
        holder.authorName.setText(unit.getAuthorName());
        holder.deleteButton.setVisibility(View.INVISIBLE);
        holder.fc_unitTint.setVisibility(View.VISIBLE);
        holder.pt_unitTint.setVisibility(View.INVISIBLE);
        if (unit.getFavoriteUsers().contains(mainActivity.owner)) {
            holder.favouriteButton.setVisibility(View.VISIBLE);
            holder.unfavouriteButton.setVisibility(View.INVISIBLE);
        }
        else {
            holder.favouriteButton.setVisibility(View.INVISIBLE);
            holder.unfavouriteButton.setVisibility(View.VISIBLE);
        }

        holder.favouriteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.favouriteButton.setVisibility(View.INVISIBLE);
                holder.unfavouriteButton.setVisibility(View.VISIBLE);
                unit.removeFavoriteUser(mainActivity.owner);
                unit.updateFavorites();
            }
        });

        holder.unfavouriteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.favouriteButton.setVisibility(View.VISIBLE);
                holder.unfavouriteButton.setVisibility(View.INVISIBLE);
                unit.addFavoriteUser(mainActivity.owner);
                unit.updateFavorites();
            }
        });

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mainActivity.setCurrentFCUnit(unit);
                mainActivity.open_screen(new FCViewFragment());
            }
        });
    }

    @Override
    public int getItemCount() {
        return FCUnits.size();
    }

    public ArrayList<FCUnit> getData() {
        return FCUnits;
    }
}
