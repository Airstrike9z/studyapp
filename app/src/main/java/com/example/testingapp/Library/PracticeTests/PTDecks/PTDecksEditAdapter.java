package com.example.testingapp.Library.PracticeTests.PTDecks;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.example.testingapp.Library.PracticeTests.PTUnit;
import com.example.testingapp.MainActivity;
import com.example.testingapp.R;

import java.util.ArrayList;

public class PTDecksEditAdapter extends RecyclerView.Adapter<PTDecksEditHolder> {

    private Context context;
    private ArrayList<PTUnit> PTUnits = new ArrayList<>();
    ArrayList<PTUnit> ptUnitsToRemove = new ArrayList<>();
    MainActivity mainActivity;
    Fragment fragment;

    public PTDecksEditAdapter(@NonNull Context context, @NonNull ArrayList<PTUnit> units, @NonNull Fragment fragment) {
        this.context = context;
        for (PTUnit unit : units) {
            PTUnits.add(unit);
        }
        this.fragment = fragment;
    }

    @NonNull
    @Override
    public PTDecksEditHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View itemView = inflater.inflate(R.layout.item_edit_unit, parent, false);
        PTDecksEditHolder viewHolder = new PTDecksEditHolder(itemView);
        return viewHolder;
    }

    public void setMainActivity(MainActivity mainActivity) {
        this.mainActivity = mainActivity;
    }

    @Override
    public void onBindViewHolder(@NonNull PTDecksEditHolder holder, int position) {
        PTUnit unit = PTUnits.get(position);
        holder.unitName.setText(unit.getUnitName());
        holder.unitName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                unit.setUnitName(s.toString());
            }
        });
        holder.authorName.setText(unit.getAuthorName());
        holder.deleteButton.setVisibility(View.VISIBLE);
        holder.fc_unitTint.setVisibility(View.INVISIBLE);
        holder.pt_unitTint.setVisibility(View.VISIBLE);

        holder.deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ptUnitsToRemove.add(unit);
                PTUnits.remove(unit);
                notifyDataSetChanged();
            }
        });

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });
    }

    @Override
    public int getItemCount() {
        return PTUnits.size();
    }

    public void updateData(ArrayList<PTUnit> ptUnits) {
        PTUnits.clear();
        for (PTUnit unit : ptUnits) {
            PTUnits.add(unit);
        }
        notifyDataSetChanged();
    }

    public void confirmEdit() {
        for (PTUnit unit : ptUnitsToRemove) {
            unit.remove();
        }
        for (PTUnit unit : PTUnits) {
            if (unit.getRef() == null) {
                mainActivity.addPTUnit(unit);
            }
            else {
                unit.update();
            }
        }
    }

    public void addData() {
        PTUnit unit = new PTUnit(null);
        unit.setAuthorName(mainActivity.owner); // whatever name is entered in StartingFragment
        unit.setUnitName("New Unit"); // default name of each new PT unit
        PTUnits.add(unit);
        notifyDataSetChanged();
    }
}
