package com.example.testingapp.Library.Flashcards;

import com.example.testingapp.MainActivity;
import com.example.testingapp.Unique.Flashcard;
import com.google.firebase.database.DatabaseReference;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class FCUnit {
    private String unitName;
    private String authorName;
    private ArrayList<Flashcard> flashcards = new ArrayList<>();
    private ArrayList<String> viewers = new ArrayList<>();
    private ArrayList<String> favoritedBy = new ArrayList<>();
    private String code;
    private DatabaseReference dbRef;

    public FCUnit(DatabaseReference dbRef){
        this.dbRef = dbRef;
    }

    public String getUnitName() {
        return unitName;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }

    public String getAuthorName() {
        return authorName;
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }

    public ArrayList<String> getFavoriteUsers() {
        return this.favoritedBy;
    }

    public ArrayList<Flashcard> getFlashcards() { return flashcards; }

    public void setFlashcards(ArrayList<Flashcard> flashcards) { this.flashcards = flashcards; }

    public void addFlashcard(Flashcard f){
        this.flashcards.add(f);
    }

    public void remove() {
        this.dbRef.removeValue();
    }

    public void update() {
        Map<String,Object> map = new HashMap<>();
        map.put(MainActivity.NAME,this.unitName);
        this.dbRef.updateChildren(map);
    }

    public void updateCode() {
        Map<String,Object> map = new HashMap<>();
        map.put(MainActivity.CODE,this.code);
        this.dbRef.updateChildren(map);
    }

    public void updateFavorites() {
        Map<String,Object> map = new HashMap<>();
        map.put(MainActivity.FAVORITE,this.favoritedBy);
        this.dbRef.updateChildren(map);
    }

    public DatabaseReference getRef() {
        return dbRef;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public void addFavoriteUser(String user) {
        this.favoritedBy.add(user);
    }

    public void removeFavoriteUser(String user) {
        this.favoritedBy.remove(user);
    }
}
