package com.example.testingapp.Library.Flashcards.FCCards;

import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.testingapp.R;

public class FCCardsViewHolder extends RecyclerView.ViewHolder {
    public FCCardsViewHolder(@NonNull View itemView) {
        super(itemView);
        this.itemView = itemView;
        questionText = itemView.findViewById(R.id.question_text);
        answerText = itemView.findViewById(R.id.answer_text);
        prof1 = itemView.findViewById(R.id.proficiency1_image);
        prof2 = itemView.findViewById(R.id.proficiency2_image);
        prof3 = itemView.findViewById(R.id.proficiency3_image);
        prof4 = itemView.findViewById(R.id.proficiency4_image);
        scrollableText = itemView.findViewById(R.id.scrollable_answer);
        scrollableText.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                v.getParent().requestDisallowInterceptTouchEvent(true);
                return false;
            }
        });
    }

    public View itemView;
    public TextView questionText;
    public TextView answerText;
    public ImageView prof1;
    public ImageView prof2;
    public ImageView prof3;
    public ImageView prof4;
    public ScrollView scrollableText;
}
