package com.example.testingapp.Library.PracticeTests.PTCards;

import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.testingapp.R;

public class PTCardsViewHolder extends RecyclerView.ViewHolder {
    public PTCardsViewHolder(@NonNull View itemView) {
        super(itemView);
        this.itemView = itemView;
        questionText = itemView.findViewById(R.id.question_text);
        answerText = itemView.findViewById(R.id.answer_text);
        scrollableText = itemView.findViewById(R.id.scrollable_answer);
        scrollableText.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                v.getParent().requestDisallowInterceptTouchEvent(true);
                return false;
            }
        });
    }

    public View itemView;
    public TextView questionText;
    public TextView answerText;
    public ScrollView scrollableText;
}
