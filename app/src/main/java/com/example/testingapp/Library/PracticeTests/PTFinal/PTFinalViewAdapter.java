package com.example.testingapp.Library.PracticeTests.PTFinal;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.example.testingapp.Library.Fragments.PTViewFragment;
import com.example.testingapp.Library.PracticeTests.PTDecks.PTDecksViewHolder;
import com.example.testingapp.Library.PracticeTests.PTUnit;
import com.example.testingapp.MainActivity;
import com.example.testingapp.R;
import com.example.testingapp.Unique.Question;

import java.util.ArrayList;

public class PTFinalViewAdapter extends RecyclerView.Adapter<PTFinalViewHolder>{

    Context context;
    ArrayList<Question> actualAnswers;
    ArrayList<String> userAnswers;
    MainActivity mainActivity;
    Fragment fragment;

    public PTFinalViewAdapter(@NonNull Context context, @NonNull ArrayList<Question> actualAnswers, @NonNull ArrayList<String> userAnswers, @NonNull Fragment fragment) {
        this.context = context;
        this.actualAnswers = actualAnswers;
        this.userAnswers = userAnswers;
        this.fragment = fragment;
    }

    @NonNull
    @Override
    public PTFinalViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View itemView = inflater.inflate(R.layout.item_pt_finished_study_card, parent, false);
        PTFinalViewHolder viewHolder = new PTFinalViewHolder(itemView);
        return viewHolder;
    }

    public void setMainActivity(MainActivity mainActivity) {
        this.mainActivity = mainActivity;
    }

    @Override
    public void onBindViewHolder(@NonNull PTFinalViewHolder holder, int position) {
        Question question = actualAnswers.get(position);
        String userAnswer = userAnswers.get(position);

        holder.questionText.setText(question.getQuestion());
        holder.yourAnswerText.setText("Your answer: " + userAnswer);
        holder.actualAnswerText.setText("Check your answer: " + question.getAnswer());
    }

    @Override
    public int getItemCount() {
        return userAnswers.size();
    }
}
