package com.example.testingapp.Library.Fragments;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.example.testingapp.Library.PracticeTests.PTCards.PTCardsViewAdapter;
import com.example.testingapp.Library.PracticeTests.PTUnit;
import com.example.testingapp.MainActivity;
import com.example.testingapp.R;

public class PTViewFragment extends Fragment {
    View view;
    PTUnit unit;
    PTCardsViewAdapter PTCardsViewAdapter;
    RecyclerView ptRecyclerView;

    public PTViewFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.practice_test_screen, container, false);
        this.unit = ((MainActivity)getActivity()).currentPTUnit;
        ImageButton back_button = view.findViewById(R.id.back_button);
        TextView unit_name = view.findViewById(R.id.unit_name);
        ImageButton share_button = view.findViewById(R.id.share_pt_button);
        ImageButton start_button = view.findViewById(R.id.start_pt_deck_button);
        ImageButton edit_button = view.findViewById(R.id.edit_pt_button);
        ImageView screen_cover = view.findViewById(R.id.screen_cover);
        ImageView friend_code_rectangle = view.findViewById(R.id.friend_code_rectangle);
        TextView friend_code_text = view.findViewById(R.id.friend_code_text);
        ImageButton cancel_button = view.findViewById(R.id.cancel_button);
        ImageButton copy_button = view.findViewById(R.id.copy_button);

        ImageButton homePageButton = view.findViewById(R.id.unhome_button);
        ImageButton timerButton = view.findViewById(R.id.untimer_button);

        unit_name.setText(unit.getUnitName());

        homePageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity)getActivity()).open_screen(new HomeFragment());
            }
        });

        timerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((MainActivity)getActivity()).timerActive) {
                    ((MainActivity)getActivity()).showTimer();
                }
                else {
                    ((MainActivity)getActivity()).open_screen(new TimerPrepFragment());
                }
            }
        });

        back_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager().popBackStack();
            }
        });

        edit_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity)getActivity()).open_screen(new PTEditFragment(unit,PTCardsViewAdapter.getData()));
            }
        });

        start_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (PTCardsViewAdapter.getData().size() != 0) {
                    ((MainActivity)getActivity()).open_screen(new PTFragment(PTCardsViewAdapter.getData(),unit.getUnitName()));
                }
                else {
                    Toast.makeText(getActivity(), "There are no questions in this test!", Toast.LENGTH_SHORT).show();
                }
            }
        });

        share_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                unit.updateCode();
                screen_cover.setVisibility(View.VISIBLE);
                friend_code_rectangle.setVisibility(View.VISIBLE);
                friend_code_text.setText(unit.getCode());
                friend_code_text.setVisibility(View.VISIBLE);
                cancel_button.setVisibility(View.VISIBLE);
                copy_button.setVisibility(View.VISIBLE);
            }
        });

        cancel_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                screen_cover.setVisibility(View.INVISIBLE);
                friend_code_rectangle.setVisibility(View.INVISIBLE);
                friend_code_text.setVisibility(View.INVISIBLE);
                cancel_button.setVisibility(View.INVISIBLE);
                copy_button.setVisibility(View.INVISIBLE);
            }
        });

        copy_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ClipboardManager clipboard = (ClipboardManager)getContext().getSystemService(Context.CLIPBOARD_SERVICE);
                ClipData clip = ClipData.newPlainText("Code copied",unit.getCode());
                clipboard.setPrimaryClip(clip);
                Toast.makeText(getActivity(), "Code copied to clipboard!", Toast.LENGTH_SHORT).show();
            }
        });

        setupRecyclerView();

        return view;
    }

    void setupRecyclerView() {
        ptRecyclerView = view.findViewById(R.id.pt_cards_list_view);
        PTCardsViewAdapter = new PTCardsViewAdapter(getActivity(), ((MainActivity)getActivity()).currentPTQuestions, this);
        PTCardsViewAdapter.setMainActivity(((MainActivity)getActivity()));
        ptRecyclerView.setAdapter(PTCardsViewAdapter); // default starting adapter for PTViewFragment
        ((MainActivity)getActivity()).addAdapter(PTCardsViewAdapter);
    }
}
