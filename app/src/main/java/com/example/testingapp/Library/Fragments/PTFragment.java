package com.example.testingapp.Library.Fragments;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.example.testingapp.MainActivity;
import com.example.testingapp.R;
import com.example.testingapp.Unique.Question;

import java.util.ArrayList;
import java.util.Collections;

public class PTFragment extends Fragment {
    View view;
    ArrayList<Question> questions = new ArrayList<>();
    ArrayList<String> currentAnswers = new ArrayList<>();
    String unitName;
    String currentAnswer;
    int currentIndex = 0;

    public PTFragment(ArrayList<Question> viewQuestions, String unitName) {
        this.unitName = unitName;
        questions.addAll(viewQuestions);
        Collections.shuffle(questions);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.pt_study_screen, container, false);

        ImageButton backButton = view.findViewById(R.id.back_button);
        TextView deckName = view.findViewById(R.id.deck_name);
        TextView question_text = view.findViewById(R.id.question_text);
        EditText cardText = view.findViewById(R.id.card_text);
        ImageButton nextButtonHighlighted = view.findViewById(R.id.next_question_button_highlighted);
        ImageButton nextButtonUnhighlighted = view.findViewById(R.id.next_question_button_unhighlighted);

        ImageButton homePageButton = view.findViewById(R.id.unhome_button);
        ImageButton timerButton = view.findViewById(R.id.untimer_button);

        homePageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity)getActivity()).open_screen(new HomeFragment());
            }
        });

        timerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((MainActivity)getActivity()).timerActive) {
                    ((MainActivity)getActivity()).showTimer();
                }
                else {
                    ((MainActivity)getActivity()).open_screen(new TimerPrepFragment());
                }
            }
        });

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager().popBackStack();
            }
        });

        deckName.setText(unitName);

        question_text.setText(questions.get(currentIndex).getQuestion());

        cardText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) { }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) { }
            @Override
            public void afterTextChanged(Editable s) {
                currentAnswer = String.valueOf(s);
                if (currentAnswer.length() > 0) {
                    nextButtonHighlighted.setVisibility(View.VISIBLE);
                    nextButtonUnhighlighted.setVisibility(View.INVISIBLE);
                }
                else {
                    nextButtonHighlighted.setVisibility(View.INVISIBLE);
                    nextButtonUnhighlighted.setVisibility(View.VISIBLE);
                }
            }
        });

        nextButtonHighlighted.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                currentAnswers.add(currentAnswer);
                currentAnswer = "";
                currentIndex++;
                if (currentIndex == questions.size()) {
                    currentIndex--;
                    ((MainActivity)getActivity()).open_screen(new PTFinalFragment(questions,currentAnswers,unitName));
                }
                else {
                    question_text.setText(questions.get(currentIndex).getQuestion());
                    nextButtonHighlighted.setVisibility(View.INVISIBLE);
                    nextButtonUnhighlighted.setVisibility(View.VISIBLE);
                    cardText.setText("");
                }
            }
        });

        return view;
    }
}
