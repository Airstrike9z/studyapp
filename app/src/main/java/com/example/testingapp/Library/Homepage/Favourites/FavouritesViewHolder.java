package com.example.testingapp.Library.Homepage.Favourites;

import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.testingapp.R;

public class FavouritesViewHolder extends RecyclerView.ViewHolder {
    public FavouritesViewHolder(@NonNull View itemView) {
        super(itemView);
        this.itemView = itemView;
        fcTint = itemView.findViewById(R.id.decoration6);
        ptTint = itemView.findViewById(R.id.decoration7);
        unitName = itemView.findViewById(R.id.unit_name);
        favouriteButton = itemView.findViewById(R.id.favourite_button);
        unfavouriteButton = itemView.findViewById(R.id.unfavourite_button);
        deckType = itemView.findViewById(R.id.deck_type);
    }

    public View itemView;
    public ImageView fcTint;
    public ImageView ptTint;
    public TextView unitName;
    public ImageButton favouriteButton;
    public ImageButton unfavouriteButton;
    public TextView deckType;
}
