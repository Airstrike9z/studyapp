package com.example.testingapp.Library.Flashcards.FCDecks;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.example.testingapp.Library.Flashcards.FCUnit;
import com.example.testingapp.MainActivity;
import com.example.testingapp.R;

import java.util.ArrayList;

public class FCDecksEditAdapter extends RecyclerView.Adapter<FCDecksEditHolder> {

    private Context context;
    private ArrayList<FCUnit> FCUnits = new ArrayList<>();
    ArrayList<FCUnit> fcUnitsToRemove = new ArrayList<>();
    MainActivity mainActivity;
    Fragment fragment;

    public FCDecksEditAdapter(@NonNull Context context, @NonNull ArrayList<FCUnit> units, @NonNull Fragment fragment) {
        this.context = context;
        for (FCUnit unit : units) {
            FCUnits.add(unit);
        }
        this.fragment = fragment;
    }

    @NonNull
    @Override
    public FCDecksEditHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View itemView = inflater.inflate(R.layout.item_edit_unit, parent, false);
        FCDecksEditHolder viewHolder = new FCDecksEditHolder(itemView);
        return viewHolder;
    }

    public void setMainActivity(MainActivity mainActivity) {
        this.mainActivity = mainActivity;
    }

    @Override
    public void onBindViewHolder(@NonNull FCDecksEditHolder holder, int position) {
        FCUnit unit = FCUnits.get(position);
        holder.unitName.setText(unit.getUnitName());
        holder.unitName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                unit.setUnitName(s.toString());
            }
        });
        holder.authorName.setText(unit.getAuthorName());
        holder.deleteButton.setVisibility(View.VISIBLE);

        holder.deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fcUnitsToRemove.add(unit);
                FCUnits.remove(unit);
                notifyDataSetChanged();
            }
        });
    }

    @Override
    public int getItemCount() {
        return FCUnits.size();
    }

    public void updateData(ArrayList<FCUnit> fcUnits) {
        FCUnits.clear();
        for (FCUnit unit : fcUnits) {
            FCUnits.add(unit);
        }
        notifyDataSetChanged();
    }

    public void confirmEdit() {
        for (FCUnit unit : fcUnitsToRemove) {
            unit.remove();
        }
        for (FCUnit unit : FCUnits) {
            if (unit.getRef() == null) {
                mainActivity.addFCUnit(unit);
            }
            else {
                unit.update();
            }
        }
    }

    public void addData() {
        FCUnit unit = new FCUnit(null);
        unit.setAuthorName(mainActivity.owner); // whatever name is entered in StartingFragment
        unit.setUnitName("New Unit"); // default name of each new FC unit
        FCUnits.add(unit);
        notifyDataSetChanged();
    }

}
