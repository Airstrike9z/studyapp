package com.example.testingapp.Library.PracticeTests;

import com.example.testingapp.MainActivity;
import com.example.testingapp.Unique.Question;
import com.google.firebase.database.DatabaseReference;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class PTUnit {
    private String unitName;
    private String authorName;
    private ArrayList<Question> questions = new ArrayList<>();
    private ArrayList<String> viewers = new ArrayList<>();
    private ArrayList<String> favoritedBy = new ArrayList<>();
    private String code;
    private DatabaseReference dbRef;

    public PTUnit(DatabaseReference dbRef){
        this.dbRef = dbRef;
    }

    public String getUnitName() { return unitName; }

    public void setUnitName(String unitName) { this.unitName = unitName; }

    public String getAuthorName() { return authorName; }

    public void setAuthorName(String authorName) { this.authorName = authorName; }

    public ArrayList<String> getFavoriteUsers() {
        return this.favoritedBy;
    }

    public ArrayList<Question> getQuestions() { return questions; }

    public void addQuestion(Question question) {
        this.questions.add(question);
    }

    public void remove() {
        this.dbRef.removeValue();
    }

    public void update() {
        Map<String,Object> map = new HashMap<>();
        map.put(MainActivity.NAME,this.unitName);
        this.dbRef.updateChildren(map);
    }

    public void updateCode() {
        Map<String,Object> map = new HashMap<>();
        map.put(MainActivity.CODE,this.code);
        this.dbRef.updateChildren(map);
    }

    public void updateFavorites() {
        Map<String,Object> map = new HashMap<>();
        map.put(MainActivity.FAVORITE,this.favoritedBy);
        this.dbRef.updateChildren(map);
    }

    public String getCode() {
        return this.code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public DatabaseReference getRef() {
        return dbRef;
    }

    public void addFavoriteUser(String user) {
        this.favoritedBy.add(user);
    }

    public void removeFavoriteUser(String user) {
        this.favoritedBy.remove(user);
    }
}
